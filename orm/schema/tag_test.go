package schema

import "testing"

func BenchmarkTag(b *testing.B) {
	for i := 0; i < b.N; i++ {
		NewTag("column:email;type:varchar;size:100;pk:true;comment:0-girl,1-boy")
	}
}
