package schema

import (
	"fmt"
	"strings"
)

const (
	TAG_BASE = "orm"

	TAG_PK      = "pk"
	TAG_AUTO    = "auto"
	TAG_TYPE    = "type"
	TAG_SIZE    = "size"
	TAG_VALID   = "valid"
	TAG_COLUMN  = "column"
	TAG_DEFAULT = "default"
	TAG_COMMENT = "comment"
)

type Tag struct {
	PK      bool
	Auto    bool
	Type    string
	Size    string
	Valid   string
	Column  string
	Default string
	Comment string
}

// NewTag parses orm tag to the *Tag.
// QEMU Virtual CPU version 2.5+
// Benchmark-1           1991818               602.9 ns/op           360 B/op          8 allocs/op
// Benchmark-2           2227740               540.0 ns/op           360 B/op          8 allocs/op
func NewTag(tag string) *Tag {
	var t Tag
	pairs := strings.Split(tag, ";")
	for _, pair := range pairs {
		kv := strings.Split(pair, ":")
		if len(kv) < 2 {
			continue
		}

		switch kv[0] {
		case TAG_PK:
			if kv[1] == "true" || kv[1] == "1" {
				t.PK = true
			}
		case TAG_AUTO:
			if kv[1] == "true" || kv[1] == "1" {
				t.Auto = true
			}
		case TAG_TYPE:
			t.Type = strings.ToUpper(kv[1])
		case TAG_SIZE:
			t.Size = kv[1]
		case TAG_COLUMN:
			t.Column = kv[1]
		case TAG_DEFAULT:
			t.Default = kv[1]
		case TAG_COMMENT:
			t.Comment = kv[1]
		default:
			panic(fmt.Sprintf("invalid orm tag: %s (%s)", kv[0], strings.Join(kv[2:], ":")))
		}
	}

	return &t
}

// IsAutoInsertField checks if the field are automatically generated data
func (own *Tag) IsAutoInsertField() bool {
	return own.Default == "CURRENT_TIMESTAMP" || own.Default == "current_timestamp" || own.Auto
}
