package schema

import (
	"reflect"
	"testing"

	"gitee.com/leminewx/gokit/orm/dialect"
)

func TestParse(t *testing.T) {
	type User struct {
		Name string `pk:"true" size:"30"`
		Age  int
	}

	var dial, _ = dialect.Get("mysql")
	schema := Parse(&User{}, dial)

	if schema.Name != "User" || len(schema.Fields) != 2 {
		t.Fatal("failed to parse User struct")
	}

	t.Log(schema.GetField("Name"))
}

func TestSchema_RecordValues(t *testing.T) {
	type User struct {
		Name string `colunm:"name" pk:"true" size:"30"`
		Sex  bool   `colunm:"sex"`
		Age  int
	}

	var dial, _ = dialect.Get("mysql")
	schema := Parse(&User{}, dial)
	values := schema.ExtractValues(&User{"Tom", true, 18})

	name := values[0].(string)
	sex := values[1].(bool)
	age := values[2].(int)

	if name != "Tom" || sex != true || age != 18 {
		t.Fatal("failed to get values")
	}

	t.Log(values...)
}

func BenchmarkSchema_Parse(b *testing.B) {
	type User struct {
		Name  string `orm:"type:varchar;size:50;pk:true" json:"name"`
		Sex   int8   `orm:"comment:0-girl,1-boy" json:"sex"`
		Age   int    `orm:"column:age" json:"age"`
		Addr  string `orm:"type:varchar;size:50" json:"addr"`
		Email string `orm:"column:email;type:varchar;size:100" json:"email"`
	}

	dial, _ := dialect.Get("mysql")
	for i := 0; i < b.N; i++ {
		Parse(&User{}, dial)
	}
}

func BenchmarkSchema_ExtractValues(b *testing.B) {
	type User struct {
		Name  string `orm:"type:varchar;size:50;pk:true" json:"name"`
		Sex   int8   `orm:"comment:0-girl,1-boy" json:"sex"`
		Age   int    `orm:"column:age" json:"age"`
		Addr  string `orm:"type:varchar;size:50" json:"addr"`
		Email string `orm:"column:email;type:varchar;size:100" json:"email"`
	}
	user := User{
		Name:  "wx",
		Sex:   1,
		Age:   31,
		Addr:  "chongqing",
		Email: "wx@cictci.com",
	}

	dial, _ := dialect.Get("mysql")
	schema := Parse(&User{}, dial)
	for i := 0; i < b.N; i++ {
		schema.ExtractValues(&user)
	}
}

func TestXxx(t *testing.T) {
	type User struct {
		Name  string `orm:"type:varchar;size:50;pk:true" json:"name"`
		Sex   int8   `orm:"comment:0-girl,1-boy" json:"sex"`
		Age   int    `orm:"column:age" json:"age"`
		Addr  string `orm:"type:varchar;size:50" json:"addr"`
		Email string `orm:"column:email;type:varchar;size:100" json:"email"`
	}

	t.Log(reflect.Indirect(reflect.ValueOf(&User{})).Type())
	t.Log(reflect.TypeOf(&User{}))
}
