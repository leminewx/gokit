package schema

import (
	"go/ast"
	"reflect"

	"gitee.com/leminewx/gokit/orm/dialect"
)

type (
	// Field represents a column of database
	Field struct {
		Name string // expresses the name of a column
		Type string // expresses the datatype of a column
		Tags *Tag   // expresses the tags of a colum in Go
	}

	// Schema represents a table of database
	Schema struct {
		Name                  string       // the table name
		Model                 any          // the model of a data object in Go (it's a Go struct).
		ModelName             string       // the name of the data model in Go.
		ModelType             reflect.Type // the type of the data model in Go.
		Fields                []*Field     // table fields
		FieldNames            []string     // the field names in Go.
		ColumnNames           []any        // the column names in DB.
		InsertableColumnNames []any        // the insertable column names in DB (remove automatically generated fields).
		fieldMap              map[string]*Field
	}
)

// GetField returns a field by the column name
func (own *Schema) GetField(name string) *Field {
	return own.fieldMap[name]
}

// ExtractValues return the values of object's member variables.
// QEMU Virtual CPU version 2.5+
// Benchmark-1           1606934               746.9 ns/op           184 B/op         11 allocs/op
// Benchmark-2           1727646               686.2 ns/op           184 B/op         11 allocs/op
func (own *Schema) ExtractValues(object any) []any {
	fieldValues := make([]any, 0, len(own.Fields))
	destValue := reflect.Indirect(reflect.ValueOf(object))
	for _, field := range own.Fields {
		if !field.Tags.IsAutoInsertField() {
			fieldValues = append(fieldValues, destValue.FieldByName(field.Name).Interface())
		}
	}

	return fieldValues
}

type TableNameGetter interface {
	GetTableName() string
}

// Parse a struct to a Schema instance.
// QEMU Virtual CPU version 2.5+
// Benchmark-1           254864              4565 ns/op            2440 B/op         54 allocs/op
// Benchmark-2           279913              4076 ns/op            2440 B/op         54 allocs/op
func Parse(model any, dial dialect.Dialect) *Schema {
	modelType := reflect.Indirect(reflect.ValueOf(model)).Type()

	// gets the table name
	var table string
	if tab, ok := model.(TableNameGetter); ok {
		table = tab.GetTableName()
	} else {
		table = modelType.Name()
	}

	// creates a schema
	schema := &Schema{
		Name:                  table,
		Model:                 model,
		ModelName:             modelType.String(),
		ModelType:             reflect.TypeOf(model),
		Fields:                make([]*Field, 0),
		FieldNames:            make([]string, 0),
		ColumnNames:           make([]any, 0),
		InsertableColumnNames: make([]any, 0),
		fieldMap:              make(map[string]*Field),
	}

	if schema.ModelType.Kind() == reflect.Ptr {
		schema.ModelType = schema.ModelType.Elem()
	}

	// parses the dest by the dialect
	for i := 0; i < modelType.NumField(); i++ {
		field := modelType.Field(i)
		if !field.Anonymous && ast.IsExported(field.Name) {
			f := &Field{
				Name: field.Name, // the field name of the Go struct
				Type: dial.GetDataTypeOf(reflect.Indirect(reflect.New(field.Type))),
			}

			// reads and parses orm tags of the data table
			if ormTags := field.Tag.Get(TAG_BASE); ormTags != "" {
				f.Tags = NewTag(ormTags)

				// assigns the field name of the data table
				if f.Tags.Column == "" {
					// uses the field name of the Go struct as the field name for the data table by default
					f.Tags.Column = f.Name
				}

				// assigns the field type of the data table
				if f.Tags.Type == "" {
					f.Tags.Type = f.Type
				} else {
					f.Type = f.Tags.Type
				}
			} else {
				f.Tags = &Tag{Type: f.Type, Column: f.Name}
			}

			schema.Fields = append(schema.Fields, f)
			schema.FieldNames = append(schema.FieldNames, f.Name)
			schema.ColumnNames = append(schema.ColumnNames, f.Tags.Column)
			if !f.Tags.IsAutoInsertField() {
				schema.InsertableColumnNames = append(schema.InsertableColumnNames, f.Tags.Column)
			}
			schema.fieldMap[f.Name] = f
		}
	}

	return schema
}
