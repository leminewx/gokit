package orm

import (
	"database/sql"
	"fmt"
	"reflect"
	"sync"

	"gitee.com/leminewx/gokit/logger"
	"gitee.com/leminewx/gokit/orm/dialect"
	"gitee.com/leminewx/gokit/orm/schema"
	"gitee.com/leminewx/gokit/orm/session"
)

const (
	_LOG_FORMAT = "ORM >>> %v"
)

// Engine is the main struct of geeorm, manages all db sessions and transactions.
type Engine struct {
	db      *sql.DB
	dbName  string
	dialect dialect.Dialect
	schemas *sync.Map
}

// NewEngine create a instance of Engine connect database and ping it to test whether it's alive.
func NewEngine(driverName, dbName, dbSource string) (eng *Engine, err error) {
	db, err := sql.Open(driverName, dbSource)
	if err != nil {
		logger.Errorf(_LOG_FORMAT, err)
		return
	}

	// send a ping to make sure the database connection is alive.
	if err = db.Ping(); err != nil {
		logger.Errorf(_LOG_FORMAT, err)
		return
	}

	// make sure the specific dialect exists.
	dial, ok := dialect.Get(driverName)
	if !ok {
		logger.Errorf(_LOG_FORMAT, "not found dialect: "+driverName)
		return nil, fmt.Errorf("not found dialect: %s", driverName)
	}

	eng = &Engine{db: db, dbName: dbName, dialect: dial, schemas: new(sync.Map)}
	logger.Infof(_LOG_FORMAT, "connect database success: "+dbSource)
	return
}

// NewSession creates a new session for next operations (recommend assigning a data model).
// QEMU Virtual CPU version 2.5+
// eg: engine.NewSession().SetModel(&User{})
// Benchmark-1       736880             1389 ns/op             904 B/op         17 allocs/op
// Benchmark-2       801439             1292 ns/op             904 B/op         17 allocs/op
// eg: engine.NewSession(&User{})
// Benchmark-1      5557647            215.2 ns/op             160 B/op          3 allocs/op
// Benchmark-2      6056534            202.6 ns/op             160 B/op          3 allocs/op
func (own *Engine) NewSession(models ...any) *session.Session {
	if len(models) == 0 {
		return session.New(own.dbName, own.db, own.dialect, nil)
	}

	modelType := reflect.TypeOf(models[0]).String()
	tab, ok := own.schemas.Load(modelType)
	if !ok {
		tab = schema.Parse(models[0], own.dialect)
		own.schemas.Store(modelType, tab)
	}

	return session.New(own.dbName, own.db, own.dialect, tab.(*schema.Schema))
}

// TxFunc will be called between tx.Begin() and tx.Commit()
type TxFunc func(*session.Session) (any, error)

// Transaction executes sql wrapped in a transaction, then automatically commit if no error occurs
func (own *Engine) Transaction(fn TxFunc) (result any, err error) {
	session := own.NewSession()
	if err = session.Begin(); err != nil {
		return nil, err
	}

	defer func() {
		if p := recover(); p != nil { // re-throw panic after Rollback
			session.Rollback()
			err = fmt.Errorf("%v", p)
			logger.Errorf(_LOG_FORMAT, err)
		} else if err != nil { // err is non-nil; don't change it
			session.Rollback()
		} else { // err is nil; if Commit returns error update err
			err = session.Commit()
		}
	}()

	return fn(session)
}

// ALTER TABLE <表名> ADD <新字段名> <数据类型> [约束条件] [first | after 已存在字段名]
// ALTER TABLE <表名> ADD COLUMN <新字段名>, <数据类型>;
// Migrate miggrates the table
// func (own *Engine) Migrate(value any) error {
// 	_, err := own.Transaction(func(s *session.Session) (result any, err error) {
// 		// sets the data model.
// 		if !s.SetModel(value).IsExistTable() {
// 			logger.Error("not exists table:", s.GetSchema().Name)
// 			return nil, s.CreateTable()
// 		}

// 		table := s.RefTable()
// 		rows, _ := s.Raw(fmt.Sprintf("SELECT * FROM %s LIMIT 1", table.Name)).QueryRows()
// 		columns, _ := rows.Columns()
// 		addCols := difference(table.FieldNames, columns)
// 		delCols := difference(columns, table.FieldNames)
// 		logger.Infof("added cols %v, deleted cols %v", addCols, delCols)

// 		for _, col := range addCols {
// 			f := table.GetField(col)
// 			sqlStr := fmt.Sprintf("ALTER TABLE %s ADD COLUMN %s %s;", table.Name, f.Name, f.Type)
// 			if _, err = s.Raw(sqlStr).Exec(); err != nil {
// 				return
// 			}
// 		}

// 		if len(delCols) == 0 {
// 			return
// 		}
// 		tmp := "tmp_" + table.Name
// 		fieldStr := strings.Join(table.FieldNames, ", ")
// 		s.Raw(fmt.Sprintf("CREATE TABLE %s AS SELECT %s from %s;", tmp, fieldStr, table.Name))
// 		s.Raw(fmt.Sprintf("DROP TABLE %s;", table.Name))
// 		s.Raw(fmt.Sprintf("ALTER TABLE %s RENAME TO %s;", tmp, table.Name))
// 		_, err = s.Exec()
// 		return
// 	})
// 	return err
// }

// // difference returns a - b
// func difference(a []string, b []string) (diff []string) {
// 	mapB := make(map[string]bool)
// 	for _, v := range b {
// 		mapB[v] = true
// 	}
// 	for _, v := range a {
// 		if _, ok := mapB[v]; !ok {
// 			diff = append(diff, v)
// 		}
// 	}
// 	return
// }

// Close closes database connection.
func (own *Engine) Close() error {
	if err := own.db.Close(); err != nil {
		logger.Errorf(_LOG_FORMAT, "failed to close database: "+err.Error())
		return err
	}

	logger.Infof(_LOG_FORMAT, "close database success")
	return nil
}
