package orm

import (
	"sync"
	"testing"

	"gitee.com/leminewx/gokit/orm/dialect"
	_ "github.com/go-sql-driver/mysql"
)

// func OpenDB(t *testing.T) *Engine {
// 	t.Helper()
// 	engine, err := NewEngine("sqlite3", "gee.db")
// 	if err != nil {
// 		t.Fatal("failed to connect", err)
// 	}
// 	return engine
// }

// func TestNewEngine(t *testing.T) {
// 	engine := OpenDB(t)
// 	defer engine.Close()
// }

// type User struct {
// 	Name string `geeorm:"PRIMARY KEY"`
// 	Age  int
// }

// func transactionRollback(t *testing.T) {
// 	engine := OpenDB(t)
// 	defer engine.Close()
// 	s := engine.NewSession()
// 	_ = s.Model(&User{}).DropTable()
// 	_, err := engine.Transaction(func(s *session.Session) (result interface{}, err error) {
// 		_ = s.Model(&User{}).CreateTable()
// 		_, err = s.Insert(&User{"Tom", 18})
// 		return nil, errors.New("Error")
// 	})
// 	if err == nil || s.HasTable() {
// 		t.Fatal("failed to rollback")
// 	}
// }

// func transactionCommit(t *testing.T) {
// 	engine := OpenDB(t)
// 	defer engine.Close()
// 	s := engine.NewSession()
// 	_ = s.Model(&User{}).DropTable()
// 	_, err := engine.Transaction(func(s *session.Session) (result interface{}, err error) {
// 		_ = s.Model(&User{}).CreateTable()
// 		_, err = s.Insert(&User{"Tom", 18})
// 		return
// 	})
// 	u := &User{}
// 	_ = s.First(u)
// 	if err != nil || u.Name != "Tom" {
// 		t.Fatal("failed to commit")
// 	}
// }

// func TestEngine_Transaction(t *testing.T) {
// 	t.Run("rollback", func(t *testing.T) {
// 		transactionRollback(t)
// 	})
// 	t.Run("commit", func(t *testing.T) {
// 		transactionCommit(t)
// 	})
// }

// func TestEngine_Migrate(t *testing.T) {
// 	engine := OpenDB(t)
// 	defer engine.Close()
// 	s := engine.NewSession()
// 	_, _ = s.Raw("DROP TABLE IF EXISTS User;").Exec()
// 	_, _ = s.Raw("CREATE TABLE User(Name text PRIMARY KEY, XXX integer);").Exec()
// 	_, _ = s.Raw("INSERT INTO User(`Name`) values (?), (?)", "Tom", "Sam").Exec()
// 	engine.Migrate(&User{})

// 	rows, _ := s.Raw("SELECT * FROM User").QueryRows()
// 	columns, _ := rows.Columns()
// 	if !reflect.DeepEqual(columns, []string{"Name", "Age"}) {
// 		t.Fatal("Failed to migrate table User, got columns", columns)
// 	}
// }

func BenchmarkEngine_NewSession(b *testing.B) {
	type User struct {
		Name string `orm:"type:varchar;size:50;pk:true" json:"name"`
	}

	dialect, _ := dialect.Get("mysql")
	engine := &Engine{
		dialect: dialect,
		schemas: new(sync.Map),
	}

	// sets the data model after each creation.
	// QEMU Virtual CPU version 2.5+
	// Benchmark-1      736880              1389 ns/op             904 B/op         17 allocs/op
	// Benchmark-2      801439              1292 ns/op             904 B/op         17 allocs/op
	// for i := 0; i < b.N; i++ {
	// 	engine.NewSession().SetModel(&User{})
	// }

	// assigns the data model every time it is created.
	// QEMU Virtual CPU version 2.5+
	// Benchmark-1      5557647               215.2 ns/op           160 B/op          3 allocs/op
	// Benchmark-2      6056534               202.6 ns/op           160 B/op          3 allocs/op
	for i := 0; i < b.N; i++ {
		engine.NewSession(&User{}).SetModel(&User{})
	}
}
