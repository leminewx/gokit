package dialect

import (
	"fmt"
	"reflect"
	"time"
)

var _ Dialect = (*mysql)(nil)

func init() {
	Register(DRIVER_NAME_MYSQL, &mysql{})
}

type mysql struct{}

func (own *mysql) GetDriverName() string {
	return DRIVER_NAME_MYSQL
}

func (own *mysql) GetDataTypeOf(typ reflect.Value) string {
	switch typ.Kind() {
	case reflect.Bool:
		return "BOOL"
	case reflect.String:
		return "VARCHAR"
	case reflect.Int8:
		return "TINYINT"
	case reflect.Int16:
		return "SMALLINT"
	case reflect.Int, reflect.Int32:
		return "INTEGER"
	case reflect.Int64:
		return "BIGINT"
	case reflect.Uint8:
		return "TINYINT UNSIGNED"
	case reflect.Uint16:
		return "SMALLINT UNSIGNED"
	case reflect.Uint, reflect.Uint32:
		return "INTEGER UNSIGNED"
	case reflect.Uint64:
		return "BIGINT UNSIGNED"
	case reflect.Float32:
		return "FLOAT"
	case reflect.Float64:
		return "REAL"
	case reflect.Array, reflect.Slice:
		return "BLOB"
	case reflect.Struct:
		if _, ok := typ.Interface().(time.Time); ok {
			return "DATETIME"
		}
		return "JSON" // not exist any type
	case reflect.Map:
		return "JSON" // not exist any type
	}

	panic(fmt.Sprintf("invalid MySQL datatype: %s (%s)", typ.Type().Name(), typ.Kind()))
}

func (own *mysql) GetTableExistSQL(dbName, tabName string) (string, []any) {
	return "SELECT table_name FROM information_schema.tables WHERE table_schema=? AND table_name=?;", []any{dbName, tabName}
}
