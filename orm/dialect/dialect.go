package dialect

import "reflect"

var dialectMap = map[string]Dialect{}

const (
	DRIVER_NAME_MYSQL      = "mysql"
	DRIVER_NAME_SQLITE3    = "sqlite3"
	DRIVER_NAME_POSTGRESQL = "postgresql"
)

// Dialect is an interface contains methods that a dialect has to implement
type Dialect interface {
	// GetDriverName gets the driver name of the DB
	GetDriverName() string
	// GetDataTypeOf gets a datatype for a DB Dialect
	GetDataTypeOf(typ reflect.Value) string
	// GetTableExistSQL returns SQL that judge whether the table exists in database
	GetTableExistSQL(dbName, tabName string) (string, []any)
}

// Register registers a dialect to the global variable
func Register(name string, dialect Dialect) {
	dialectMap[name] = dialect
}

// Get gets the dialect from global variable if it exists
func Get(name string) (Dialect, bool) {
	dialect, ok := dialectMap[name]
	return dialect, ok
}
