package dialect

import (
	"fmt"
	"reflect"
	"time"
)

var _ Dialect = (*sqlite3)(nil)

func init() {
	Register(DRIVER_NAME_SQLITE3, &sqlite3{})
}

type sqlite3 struct{}

func (own *sqlite3) GetDriverName() string {
	return DRIVER_NAME_SQLITE3
}

func (own *sqlite3) GetDataTypeOf(typ reflect.Value) string {
	switch typ.Kind() {
	case reflect.Bool:
		return "BOOL"
	case reflect.String:
		return "TEXT"
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uintptr:
		return "INTEGER"
	case reflect.Int64, reflect.Uint64:
		return "BIGINT"
	case reflect.Float32, reflect.Float64:
		return "REAL"
	case reflect.Array, reflect.Slice:
		return "BLOB"
	case reflect.Struct:
		if _, ok := typ.Interface().(time.Time); ok {
			return "DATETIME"
		}
	}

	panic(fmt.Sprintf("invalid sqlite datatype: %s (%s)", typ.Type().Name(), typ.Kind()))
}

func (own *sqlite3) GetTableExistSQL(_, tabName string) (string, []any) {
	return "SELECT name FROM sqlite_master WHERE type='table' and name=?;", []any{tabName}
}
