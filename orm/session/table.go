package session

import (
	"fmt"
	"reflect"
	"strings"

	"gitee.com/leminewx/gokit/logger"
	"gitee.com/leminewx/gokit/orm/dialect"
	"gitee.com/leminewx/gokit/orm/schema"
)

// SetModel assigns a schema
func (own *Session) SetModel(model any) *Session {
	if own.schema == nil || reflect.TypeOf(model) != own.schema.ModelType {
		own.schema = schema.Parse(model, own.dialect)
		own.statement.WithTableName(own.schema.Name)
	}

	return own
}

// GetSchema returns a Schema instance that contains all parsed fields
func (own *Session) GetSchema() *schema.Schema {
	if own.schema == nil {
		logger.Errorf(_LOG_ERR_FORMAT, "no data model specified")
	}

	return own.schema
}

// CreateTable creates a table in database with a data model
func (own *Session) CreateTable() error {
	var pks []string
	var builder strings.Builder

	first := true
	schema := own.GetSchema()
	driverName := own.dialect.GetDriverName()
	builder.WriteString("CREATE TABLE ")
	builder.WriteString(schema.Name)
	builder.WriteString(" (")
	for _, field := range schema.Fields {
		tags := field.Tags

		// assigns this field as the primary key for the data table
		if tags.PK {
			pks = append(pks, tags.Column)
		}

		// assigns the length of the string field
		var dataType string
		if tags.Size != "" {
			dataType = fmt.Sprintf("%s(%s)", tags.Type, tags.Size)
		} else {
			dataType = field.Type
		}

		// assigns the default value of the field
		var defaultVal string
		switch tags.Default {
		case "":
		case "notnull", "not null", "NOT NULL":
			defaultVal = " NOT NULL"
		default:
			if tags.Type == "VARCHAR" || tags.Type == "TEXT" || tags.Type == "LONGTEXT" || tags.Type == "TINYTEXT" {
				defaultVal = fmt.Sprintf(" DEFAULT '%s'", tags.Default)
			} else {
				defaultVal = fmt.Sprintf(" DEFAULT %s", tags.Default)
			}
		}

		if !first {
			builder.WriteString(", ")
		}

		// builds the column clause SQL
		builder.WriteString(tags.Column)
		builder.WriteByte(' ')
		builder.WriteString(dataType)
		builder.WriteString(defaultVal)

		if tags.PK && tags.Auto {
			builder.WriteString(" AUTO_INCREAMENT")
		}

		if driverName != dialect.DRIVER_NAME_SQLITE3 && tags.Comment != "" { // sqlite is not supported COMMENT clause
			builder.WriteString(" COMMENT '")
			builder.WriteString(tags.Comment)
			builder.WriteString("'")
		}
		first = false
	}

	// assigns the primary keys of the data table
	if len(pks) > 0 {
		builder.WriteString(", PRIMARY KEY (")
		builder.WriteString(strings.Join(pks, ", "))
		builder.WriteByte(')')
	}

	// creates the data table
	builder.WriteString(");")
	_, err := own.Exec(builder.String())
	return err
}

// DropTable drops a table with the name of data model
func (own *Session) DropTable() error {
	_, err := own.Exec("DROP TABLE IF EXISTS " + own.GetSchema().Name + ";")
	return err
}

// IsExistTable returns true of the table exists
func (own *Session) IsExistTable() bool {
	sql, vals := own.dialect.GetTableExistSQL(own.dbName, own.GetSchema().Name)
	row := own.QueryRow(sql, vals...)

	var tmp string
	_ = row.Scan(&tmp)
	return tmp == own.GetSchema().Name
}
