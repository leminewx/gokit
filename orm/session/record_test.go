package session

import (
	"database/sql"
	"testing"

	"gitee.com/leminewx/gokit/orm/dialect"
	"gitee.com/leminewx/gokit/orm/statement/where"
)

func TestSession_Insert(t *testing.T) {
	TestDB, _ := sql.Open("mysql", "root:mysql#123@tcp(10.254.249.16:3306)/test?parseTime=true&loc=Local")
	defer TestDB.Close()
	testDial, _ := dialect.Get("mysql")

	session := New("test", TestDB, testDial, nil).SetModel(&TestUser{})
	if _, err := session.Insert(
		&TestUser{Name: "wx", Sex: 1, Age: 17, Addr: "chongqing.china", Email: "xxxxxxx@cictci.com", Extra: []byte{'0', '1', '2'}},
		&TestUser{Name: "dsc", Sex: 1, Age: 18, Addr: "beijing.china", Email: "xxxxxxx@cictci.com", Extra: []byte{'0', '1', '2'}},
		&TestUser{Name: "jjh", Sex: 0, Age: 19, Addr: "wuhan.china", Email: "xxxxxxx@cictci.com", Extra: []byte{'0', '1', '2'}},
		&TestUser{Name: "gh", Sex: 1, Age: 20, Addr: "chongqing.china", Email: "xxxxxxx@cictci.com", Extra: []byte{'0', '1', '2'}},
		&TestUser{Name: "hgc", Sex: 1, Age: 21, Addr: "beijing.china", Email: "xxxxxxx@cictci.com", Extra: []byte{'0', '1', '2'}},
		&TestUser{Name: "cj", Sex: 1, Age: 22, Addr: "wuhan.china", Email: "xxxxxxx@cictci.com", Extra: []byte{'0', '1', '2'}},
		&TestUser{Name: "yxs", Sex: 1, Age: 23, Addr: "chongqing.china", Email: "xxxxxxx@cictci.com", Extra: []byte{'0', '1', '2'}},
		&TestUser{Name: "zfr", Sex: 1, Age: 24, Addr: "beijing.china", Email: "xxxxxxx@cictci.com", Extra: []byte{'0', '1', '2'}},
		&TestUser{Name: "zjl", Sex: 1, Age: 25, Addr: "wuhan.china", Email: "xxxxxxx@cictci.com", Extra: []byte{'0', '1', '2'}},
		&TestUser{Name: "zp", Sex: 1, Age: 26, Addr: "chongqing.china", Email: "xxxxxxx@cictci.com", Extra: []byte{'0', '1', '2'}},
		&TestUser{Name: "hxq", Sex: 0, Age: 27, Addr: "beijing.china", Email: "xxxxxxx@cictci.com", Extra: []byte{'0', '1', '2'}},
		&TestUser{Name: "xsl", Sex: 1, Age: 30, Addr: "wuhan.china", Email: "xxxxxxx@cictci.com", Extra: []byte{'0', '1', '2'}},
		&TestUser{Name: "yjy", Sex: 0, Age: 31, Addr: "chongqing.china", Email: "xxxxxxx@cictci.com", Extra: []byte{'0', '1', '2'}},
	); err != nil {
		t.Fatal(err)
	}
}

func TestSession_Select(t *testing.T) {
	TestDB, _ := sql.Open("mysql", "root:mysql#123@tcp(10.254.249.16:3306)/test?parseTime=true&loc=Local")
	defer TestDB.Close()
	testDial, _ := dialect.Get("mysql")

	var users []TestUser
	session := New("test", TestDB, testDial, nil).SetModel(&TestUser{})
	sess := session.Where(where.LT("age", 25), where.IN("name", "wx", "gh", "cj", "dsc"))
	count, err := sess.Count()
	if err != nil {
		t.Fatal(err)
	}
	t.Log("count:", count)

	if err := sess.Limit(1, 2).Select(&users); err != nil {
		t.Fatal(err)
	}
	for _, user := range users {
		t.Log(user)
	}
}

func TestSession_SelectFirst(t *testing.T) {
	TestDB, _ := sql.Open("mysql", "root:mysql#123@tcp(10.254.249.16:3306)/test?parseTime=true&loc=Local")
	defer TestDB.Close()
	testDial, _ := dialect.Get("mysql")

	var user TestUser
	session := New("test", TestDB, testDial, nil).SetModel(&TestUser{}).Where(where.GT("age", 25), where.IN("name", "zp", "wx", "gh")).Limit(5)
	if err := session.SelectFirst(&user); err != nil {
		t.Fatal(err)
	}
	t.Log(user)
}

func TestSession_Limit(t *testing.T) {
	TestDB, _ := sql.Open("mysql", "root:mysql#123@tcp(10.254.249.16:3306)/test?parseTime=true&loc=Local")
	defer TestDB.Close()
	testDial, _ := dialect.Get("mysql")

	session := New("test", TestDB, testDial, nil)
	var users []TestUser
	if err := session.SetModel(&TestUser{}).Limit(1).Select(&users); err != nil {
		t.Fatal(err)
	} else if len(users) != 1 {
		t.Fatal("failed to query with limit condition")
	}

	t.Log(users)
}

func TestSession_OrderBy(t *testing.T) {
	TestDB, _ := sql.Open("mysql", "root:mysql#123@tcp(10.254.249.16:3306)/test?parseTime=true&loc=Local")
	defer TestDB.Close()
	testDial, _ := dialect.Get("mysql")

	session := New("test", TestDB, testDial, nil)
	var users []TestUser
	if err := session.SetModel(&TestUser{}).OrderBy("sex", "asc").Select(&users); err != nil {
		t.Fatal(err)
	}
	for _, user := range users {
		t.Log(user)
	}

	users = []TestUser{}
	if err := session.SetModel(&TestUser{}).OrderBy("sex", "desc", "age").Select(&users); err != nil {
		t.Fatal(err)
	}
	for _, user := range users {
		t.Log(user)
	}
}

func TestSession_Update(t *testing.T) {
	TestDB, _ := sql.Open("mysql", "root:mysql#123@tcp(10.254.249.16:3306)/test?parseTime=true&loc=Local")
	defer TestDB.Close()
	testDial, _ := dialect.Get("mysql")

	session := New("test", TestDB, testDial, nil)
	affected, err := session.SetModel(&TestUser{}).Where(where.EQ("name", "wx")).Update("Age", 31)
	if err != nil {
		t.Fatal(err)
	}

	var user TestUser
	if err := session.SelectFirst(&user); err != nil {
		t.Fatal(err)
	}

	if affected != 1 {
		t.Fatal("failed to update")
	}

	t.Log(user)
}

func TestSession_DeleteAndCount(t *testing.T) {
	TestDB, _ := sql.Open("mysql", "root:mysql#123@tcp(10.254.249.16:3306)/test?parseTime=true&loc=Local")
	defer TestDB.Close()
	testDial, _ := dialect.Get("mysql")

	session := New("test", TestDB, testDial, nil)
	affected, err := session.SetModel(&TestUser{}).Where(where.EQ("name", "gh")).Delete()
	if err != nil {
		t.Fatal(err)
	}

	if affected != 1 {
		t.Fatal("failed to delete or count")
	}
}
