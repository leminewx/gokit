package session

import (
	"database/sql"

	"gitee.com/leminewx/gokit/logger"
	"gitee.com/leminewx/gokit/orm/dialect"
	"gitee.com/leminewx/gokit/orm/schema"
	"gitee.com/leminewx/gokit/orm/statement"
)

const (
	_LOG_FORMAT     = "ORM >>> %s => %v"
	_LOG_ERR_FORMAT = "ORM >>> %v"
)

// Session keep a pointer to sql.DB and provides all execution of all kind of database operations.
type Session struct {
	dbName    string
	db        *sql.DB
	tx        *sql.Tx
	schema    *schema.Schema
	dialect   dialect.Dialect
	statement *statement.Statement
	// sqlBuilder strings.Builder
	// sqlVars    []any
}

// New creates a instance of Session.
func New(dbName string, db *sql.DB, dial dialect.Dialect, schema *schema.Schema) *Session {
	return &Session{
		dbName:    dbName,
		db:        db,
		dialect:   dial,
		schema:    schema,
		statement: statement.NewStatement(""),
	}
}

// Clear initializes the state of a session.
func (own *Session) Clear() {
	own.statement.Reset(own.schema.Name)
}

// BaseDB is a minimal function set of db.
type BaseDB interface {
	Exec(query string, args ...any) (sql.Result, error)
	Query(query string, args ...any) (*sql.Rows, error)
	QueryRow(query string, args ...any) *sql.Row
}

var (
	_ BaseDB = (*sql.DB)(nil)
	_ BaseDB = (*sql.Tx)(nil)
)

// GetDB returns tx if a *sql.TX begins, otherwise returns *sql.DB.
func (own *Session) GetDB() BaseDB {
	if own.tx != nil {
		return own.tx
	}

	return own.db
}

// Exec executes a raw sql with variables.
func (own *Session) Exec(query string, args ...any) (result sql.Result, err error) {
	logger.Infof(_LOG_FORMAT, query, args)
	if result, err = own.GetDB().Exec(query, args...); err != nil {
		logger.Errorf(_LOG_ERR_FORMAT, err)
	}
	return
}

// Query gets a list of records from db.
func (own *Session) Query(query string, args ...any) (rows *sql.Rows, err error) {
	logger.Infof(_LOG_FORMAT, query, args)
	if rows, err = own.GetDB().Query(query, args...); err != nil {
		logger.Errorf(_LOG_ERR_FORMAT, err)
	}
	return
}

// QueryRow gets a record from db.
func (own *Session) QueryRow(query string, args ...any) *sql.Row {
	logger.Infof(_LOG_FORMAT, query, args)
	return own.GetDB().QueryRow(query, args...)
}

// // AppendClause appends a SQL clause and it's arguments into the SQL statement.
// func (own *Session) AppendClause(sql string, args ...any) *Session {
// 	own.sqlBuilder.WriteString(sql)
// 	own.sqlBuilder.WriteByte(' ')
// 	own.sqlVars = append(own.sqlVars, args...)
// 	return own
// }
