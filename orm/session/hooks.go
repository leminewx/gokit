package session

import (
	"reflect"

	"gitee.com/leminewx/gokit/logger"
)

// Hooks constants
const (
	HOOK_AFTER_SELECT  = "CallAfterSelect"
	HOOK_AFTER_UPDATE  = "CallAfterUpdate"
	HOOK_AFTER_DELETE  = "CallAfterDelete"
	HOOK_AFTER_INSERT  = "CallAfterInsert"
	HOOK_BEFORE_SELECT = "CallBeforeSelect"
	HOOK_BEFORE_UPDATE = "CallBeforeUpdate"
	HOOK_BEFORE_DELETE = "CallBeforeDelete"
	HOOK_BEFORE_INSERT = "CallBeforeInsert"
)

// CallMethod calls the registered hooks
func (own *Session) CallMethod(method string, value any) {
	fn := reflect.ValueOf(own.GetSchema().Model).MethodByName(method)
	if value != nil {
		fn = reflect.ValueOf(value).MethodByName(method)
	}

	param := []reflect.Value{reflect.ValueOf(own)}
	if !fn.IsValid() {
		return
	}

	if v := fn.Call(param); len(v) > 0 {
		if err, ok := v[0].Interface().(error); ok {
			logger.Errorf(_LOG_ERR_FORMAT, err)
		}
	}
}
