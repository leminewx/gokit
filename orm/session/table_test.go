package session

import (
	"database/sql"
	"reflect"
	"testing"
	"time"

	"gitee.com/leminewx/gokit/orm/dialect"
)

type TestUser struct {
	Name    string    `orm:"size:50;pk:true;comment:user name" json:"name"`
	Sex     int8      `orm:"default:not null;comment:0-girl, 1-boy" json:"sex"`
	Age     int       `orm:"column:age" json:"age"`
	Addr    string    `orm:"type:text" json:"addr"`
	Email   string    `orm:"column:email;type:varchar;size:100" json:"email"`
	AddedAt time.Time `orm:"default:CURRENT_TIMESTAMP"`
	Extra   []byte    `orm:"column:extra"`
}

func (own *TestUser) GetTableName() string {
	return "user"
}

func TestSetModule(t *testing.T) {
	var users []*TestUser
	destSlice := reflect.Indirect(reflect.ValueOf(users))
	t.Log(destSlice.Type().Elem())

	t.Log(reflect.TypeOf(TestUser{}))
	t.Log(reflect.TypeOf(&TestUser{}))
}

func TestSession_CreateTable(t *testing.T) {
	TestDB, _ := sql.Open("mysql", "root:mysql#123@tcp(10.254.249.16:3306)/test?parseTime=true&loc=Local")
	defer TestDB.Close()
	testDial, _ := dialect.Get("mysql")

	session := New("test", TestDB, testDial, nil).SetModel(&TestUser{})
	_ = session.DropTable()
	_ = session.CreateTable()
	if !session.IsExistTable() {
		t.Fatal("Failed to create table User")
	}
}

func TestSession_SetModel(t *testing.T) {
	TestDB, _ := sql.Open("mysql", "root:mysql#123@tcp(10.254.249.16:3306)/test?parseTime=true&loc=Local")
	defer TestDB.Close()
	testDial, _ := dialect.Get("mysql")

	session := New("test", TestDB, testDial, nil).SetModel(&TestUser{})
	table := session.GetSchema()
	if table.Name != "user" || session.GetSchema().Name != "user" {
		t.Fatal("Failed to change model")
	}
}
