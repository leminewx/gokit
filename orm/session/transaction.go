package session

import "gitee.com/leminewx/gokit/logger"

// Begin begins a transaction.
func (own *Session) Begin() (err error) {
	logger.Infof(_LOG_FORMAT, "begin transaction")
	if own.tx, err = own.db.Begin(); err != nil {
		logger.Errorf(_LOG_ERR_FORMAT, err)
	}
	return
}

// Commit commits a transaction.
func (own *Session) Commit() (err error) {
	logger.Infof(_LOG_FORMAT, "commit transaction")
	if err = own.tx.Commit(); err != nil {
		logger.Errorf(_LOG_ERR_FORMAT, err)
	}
	return
}

// Rollback rollbacks a transaction.
func (own *Session) Rollback() (err error) {
	logger.Infof(_LOG_FORMAT, "rollback transaction")
	if err = own.tx.Rollback(); err != nil {
		logger.Errorf(_LOG_ERR_FORMAT, err)
	}
	return
}
