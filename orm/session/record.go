package session

import (
	"errors"
	"fmt"
	"reflect"

	"gitee.com/leminewx/gokit/orm/statement"
)

// Insert inserts one or more records into database
func (own *Session) Insert(values ...any) (int64, error) {
	if len(values) == 0 {
		return 0, fmt.Errorf("insert empty values")
	} else if own.schema == nil {
		return 0, fmt.Errorf("not set data model")
	}

	// calls CallBeforeInsert method and extracts the values
	recordValues := make([]any, 0, len(values))
	for _, value := range values {
		own.CallMethod(HOOK_BEFORE_INSERT, value)
		recordValues = append(recordValues, own.schema.ExtractValues(value))
	}

	// builds the SELECT SQL then executes it
	own.statement.WithClause(statement.CLAUSE_INSERT, own.schema.InsertableColumnNames...)
	own.statement.WithClause(statement.CLAUSE_VALUES, recordValues...)
	sql, args := own.statement.Build(statement.CLAUSE_INSERT, statement.CLAUSE_VALUES)
	result, err := own.Exec(sql, args...)
	if err != nil {
		return 0, err
	}

	// calls CallAfterInsert method
	own.CallMethod(HOOK_AFTER_INSERT, nil)
	return result.RowsAffected()
}

// Select gets all eligible records
func (own *Session) Select(values any) error {
	if own.schema == nil {
		return fmt.Errorf("not set data model")
	}

	// call the CallBeforeSelect method
	own.CallMethod(HOOK_BEFORE_SELECT, nil)

	// parses the object model
	destSlice := reflect.Indirect(reflect.ValueOf(values))
	destType := destSlice.Type().Elem()
	if destType.Kind() == reflect.Ptr {
		destType = destType.Elem()
	}
	if destType != own.schema.ModelType {
		fmt.Println(destType, own.schema.ModelType)
		return fmt.Errorf("inconsistent data model")
	}

	// builds the SELECT SQL then executes it
	own.statement.WithClause(statement.CLAUSE_SELECT, own.schema.ColumnNames...)
	sql, args := own.statement.Build(statement.CLAUSE_SELECT, statement.CLAUSE_FROM, statement.CLAUSE_WHERE, statement.CLAUSE_GROUPBY, statement.CLAUSE_HAVING, statement.CLAUSE_ORDERBY, statement.CLAUSE_LIMIT)
	rows, err := own.Query(sql, args...)
	if err != nil {
		return err
	}

	for rows.Next() {
		var values []any
		dest := reflect.New(destType).Elem()
		for _, name := range own.schema.FieldNames {
			values = append(values, dest.FieldByName(name).Addr().Interface())
		}

		if err := rows.Scan(values...); err != nil {
			return err
		}

		// calls the CallAfterSelect method that executes once on each record
		own.CallMethod(HOOK_AFTER_SELECT, dest.Addr().Interface())
		destSlice.Set(reflect.Append(destSlice, dest))
	}

	if err := rows.Err(); err != nil {
		return err
	}

	return rows.Close()
}

// SelectFirst selects the 1st record
func (own *Session) SelectFirst(value any) error {
	dest := reflect.Indirect(reflect.ValueOf(value))
	destSlice := reflect.New(reflect.SliceOf(dest.Type())).Elem()
	if err := own.Limit(1).Select(destSlice.Addr().Interface()); err != nil {
		return err
	} else if destSlice.Len() == 0 {
		return errors.New("not found data")
	}

	dest.Set(destSlice.Index(0))
	return nil
}

// Update updates records with where clause, support map[string]any also support kv list: "Name", "Tom", "Age", 18, ....
func (own *Session) Update(values ...any) (int64, error) {
	// calls the CallBeforeUpdate method
	own.CallMethod(HOOK_BEFORE_UPDATE, nil)

	// builds the UPDATE SQL then executes it
	own.statement.WithClause(statement.CLAUSE_UPDATE, values...)
	sql, args := own.statement.Build(statement.CLAUSE_UPDATE, statement.CLAUSE_WHERE)
	result, err := own.Exec(sql, args...)
	if err != nil {
		return 0, err
	}

	// calls the CallAfterUpdate method
	own.CallMethod(HOOK_AFTER_UPDATE, nil)
	return result.RowsAffected()
}

// Delete records with where clause
func (own *Session) Delete() (int64, error) {
	// calls the CallBeforeDelete method
	own.CallMethod(HOOK_BEFORE_DELETE, nil)

	// builds the DELETE SQL then executes it
	own.statement.WithClause(statement.CLAUSE_DELETE)
	sql, args := own.statement.Build(statement.CLAUSE_DELETE, statement.CLAUSE_WHERE)
	result, err := own.Exec(sql, args...)
	if err != nil {
		return 0, err
	}

	// calls the CallAfterDelte method
	own.CallMethod(HOOK_AFTER_DELETE, nil)
	return result.RowsAffected()
}

// Count counts records with where clause
func (own *Session) Count() (int64, error) {
	// builds the COUNT SQL then executes it
	own.statement.WithClause(statement.CLAUSE_COUNT)
	sql, args := own.statement.Build(statement.CLAUSE_COUNT, statement.CLAUSE_WHERE)
	row := own.QueryRow(sql, args...)

	var count int64
	err := row.Scan(&count)
	return count, err
}

// Where adds a where clause to statement
func (own *Session) Where(conditions ...any) *Session {
	own.statement.WithClause(statement.CLAUSE_WHERE, conditions...)
	return own
}

// GroupBy adds a group clause to statement
func (own *Session) GroupBy(fields ...any) *Session {
	own.statement.WithClause(statement.CLAUSE_GROUPBY, fields...)
	return own
}

// OrderBy adds a order clause to statement
func (own *Session) OrderBy(orders ...any) *Session {
	own.statement.WithClause(statement.CLAUSE_ORDERBY, orders...)
	return own
}

// Limit adds a limit clause to statement
func (own *Session) Limit(limits ...any) *Session {
	own.statement.WithClause(statement.CLAUSE_LIMIT, limits...)
	return own
}
