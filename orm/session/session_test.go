package session

import (
	"database/sql"
	"strings"
	"testing"

	"gitee.com/leminewx/gokit/orm/dialect"
)

func TestSession_Exec(t *testing.T) {
	TestDB, _ := sql.Open("mysql", "root:mysql#123@tcp(10.254.249.16:3306)/test?parseTime=true&loc=Local")
	defer TestDB.Close()
	testDial, _ := dialect.Get("mysql")

	session := New("test", TestDB, testDial, nil)
	_, _ = session.GetDB().Exec("DROP TABLE IF EXISTS User;")
	_, _ = session.GetDB().Exec("CREATE TABLE User(Name text);")
	result, _ := session.GetDB().Exec("INSERT INTO User(`Name`) values (?), (?);", "Tom", "Sam")
	if count, err := result.RowsAffected(); err != nil || count != 2 {
		t.Fatal("expect 2, but got", count)
	}
}

func TestSession_QueryRows(t *testing.T) {
	TestDB, _ := sql.Open("mysql", "root:mysql#123@tcp(10.254.249.16:3306)/test?parseTime=true&loc=Local")
	defer TestDB.Close()
	testDial, _ := dialect.Get("mysql")

	session := New("test", TestDB, testDial, nil)
	// _, _ = session.AppendClause("DROP TABLE IF EXISTS User;").Exec()
	// _, _ = session.AppendClause("CREATE TABLE User(Name text);").Exec()
	row, err := session.GetDB().Query("SELECT Name, Sex, age, Addr, email FROM User LIMIT ?", 1)
	if err != nil {
		if row, err = session.GetDB().Query("SELECT Name, Sex, age, Addr, email FROM User LIMIT ?", 1); err != nil {
			t.Fatal(err)
		}

	}

	var count int
	if err := row.Scan(&count); err != nil || count != 0 {
		t.Fatal("failed to query db", err)
	}
}

func BenchmarkNewBuilder(b *testing.B) {
	for i := 0; i < b.N; i++ {
		var builder strings.Builder
		builder.WriteByte('@')
	}
}
