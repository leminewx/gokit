package where

import "testing"

func TestGT(t *testing.T) {
	t.Log(GT("a", 123))
	t.Log(GT("a", 1.2))
	t.Log(GT("a", 123, 123))
	t.Log(GT("a", 1.2, 1.2))
	t.Log(GT("a", 123, 100))
	t.Log(GT("a", 1.2, 1.0))
}

func TestGTE(t *testing.T) {
	t.Log(GTE("a", 123))
	t.Log(GTE("a", 1.2))
	t.Log(GTE("a", 123, 123))
	t.Log(GTE("a", 1.2, 1.2))
	t.Log(GTE("a", 123, 100))
	t.Log(GTE("a", 1.2, 1.0))
}

func TestLT(t *testing.T) {
	t.Log(LT("a", 123))
	t.Log(LT("a", 1.2))
	t.Log(LT("a", 123, 123))
	t.Log(LT("a", 1.2, 1.2))
	t.Log(LT("a", 123, 100))
	t.Log(LT("a", 1.2, 1.0))
}

func TestLTE(t *testing.T) {
	t.Log(LTE("a", 123))
	t.Log(LTE("a", 1.2))
	t.Log(LTE("a", 123, 123))
	t.Log(LTE("a", 1.2, 1.2))
	t.Log(LTE("a", 123, 100))
	t.Log(LTE("a", 1.2, 1.0))
}

func TestEQ(t *testing.T) {
	t.Log(EQ("a", 123))
	t.Log(EQ("a", 1.2))
	t.Log(EQ("a", 123, 123))
	t.Log(EQ("a", 1.2, 1.2))
	t.Log(EQ("a", 123, 100))
	t.Log(EQ("a", 1.2, 1.0))
}

func TestNEQ(t *testing.T) {
	t.Log(NEQ("a", 123))
	t.Log(NEQ("a", 1.2))
	t.Log(NEQ("a", 123, 123))
	t.Log(NEQ("a", 1.2, 1.2))
	t.Log(NEQ("a", 123, 100))
	t.Log(NEQ("a", 1.2, 1.0))
}

func TestIN(t *testing.T) {
	t.Log(IN("a"))
	t.Log(IN("a", 1))
	t.Log(IN("a", 1, 2, 3))
	t.Log(IN("a", "a"))
	t.Log(IN("a", "a", "b"))
}

func TestNOTIN(t *testing.T) {
	t.Log(NOTIN("a"))
	t.Log(NOTIN("a", 1))
	t.Log(NOTIN("a", 1, 2, 3))
	t.Log(NOTIN("a", "a"))
	t.Log(NOTIN("a", "a", "b"))
}

func TestLIKE(t *testing.T) {
	t.Log(LIKE("a", ""))
	t.Log(LIKE("a", "123"))
	t.Log(LIKE("a", "%123"))
	t.Log(LIKE("a", "123%"))
	t.Log(LIKE("a", "%123%"))
}

func TestBETWEEN(t *testing.T) {
	t.Log(BETWEEN("a", nil, nil))
	t.Log(BETWEEN("a", 1, 2))
	t.Log(BETWEEN("a", 1, 2))
	t.Log(BETWEEN("a", "b", nil))
	t.Log(BETWEEN("a", "a", "b"))
}

func TestNOTBETWEEN(t *testing.T) {
	t.Log(NOTBETWEEN("a", nil, nil))
	t.Log(NOTBETWEEN("a", 1, 2))
	t.Log(NOTBETWEEN("a", 1, 2))
	t.Log(NOTBETWEEN("a", "b", nil))
	t.Log(NOTBETWEEN("a", "a", "b"))
}

func TestAND(t *testing.T) {
	t.Log(AND(GT("a", 1)))
	t.Log(AND(GT("a", 1, 1)))
	t.Log(AND(LT("a", 1)))
	t.Log(AND(LT("a", 1, 1)))
	t.Log(AND(EQ("a", 1)))
	t.Log(AND(EQ("a", 1, 1)))
	t.Log(AND(IN("a", 1)))
	t.Log(AND(LIKE("a", "%abc")))
	t.Log(AND(BETWEEN("a", 1, 100)))
}

func TestOR(t *testing.T) {
	t.Log(OR(GT("a", 1)))
	t.Log(OR(GT("a", 1, 1)))
	t.Log(OR(LT("a", 1)))
	t.Log(OR(LT("a", 1, 1)))
	t.Log(OR(EQ("a", 1)))
	t.Log(OR(EQ("a", 1, 1)))
	t.Log(OR(IN("a", 1)))
	t.Log(OR(LIKE("a", "%%abc")))
	t.Log(OR(BETWEEN("a", 1, 100)))
}
