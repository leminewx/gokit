package where

import "strings"

const __SQL_COMMA = ", "

type Number interface {
	int | int8 | int16 | int32 | int64 | uint | uint8 | uint16 | uint32 | uint64 | float32 | float64
}

type Where struct {
	Desc string
	Args []any
}

// field > value
func GT(field string, value any, ignore ...any) *Where {
	return judgment(field, ">?", value, ignore)
}

// field >= value
func GTE(field string, value any, ignore ...any) *Where {
	return judgment(field, ">=?", value, ignore)
}

// field < value
func LT(field string, value any, ignore ...any) *Where {
	return judgment(field, "<?", value, ignore)

}

// field <= value
func LTE(field string, value any, ignore ...any) *Where {
	return judgment(field, "<=?", value, ignore)
}

// field = value
func EQ(field string, value any, ignore ...any) *Where {
	return judgment(field, "=?", value, ignore)
}

// field != value
func NEQ(field string, value any, ignore ...any) *Where {
	return judgment(field, "!=?", value, ignore)
}

func judgment(field, condition string, value any, ignore ...any) *Where {
	if value == nil {
		return &Where{"", []any{}}
	}
	if len(ignore) > 0 {
		for _, i := range ignore {
			if i == value {
				return &Where{"", []any{}}
			}
		}
	}

	return &Where{field + condition, []any{value}}

}

// field LIKE value
func LIKE(field string, value string) *Where {
	if value == "" {
		return &Where{"", []any{}}
	}

	return &Where{field + " LIKE ?", []any{value}}
}

// field IN (values)
func IN(field string, values ...any) *Where {
	return in(field, " IN (", values)
}

// field NOT IN (values)
func NOTIN(field string, values ...any) *Where {
	return in(field, " NOT IN (", values)
}

func in(field, condition string, values []any) *Where {
	if len(values) == 0 {
		return &Where{"", []any{}}
	}

	return &Where{field + condition + genBindVariables(len(values)) + ")", values}
}

// (field BETWEEN from AND to)
func BETWEEN(field string, from, to any) *Where {
	return between(field, " BETWEEN ? AND ?)", from, to)
}

// (field NOT BETWEEN from AND to)
func NOTBETWEEN(field string, from, to any) *Where {
	return between(field, " NOT BETWEEN ? AND ?)", from, to)
}

func between(field, condition string, from, to any) *Where {
	if from == nil || to == nil {
		return &Where{"", []any{}}
	}

	return &Where{"(" + field + condition, []any{from, to}}
}

func AND(Where *Where) *Where {
	return andOr(Where.Desc, "AND ", Where.Args)
}

func OR(Where *Where) *Where {
	return andOr(Where.Desc, "OR ", Where.Args)
}

func andOr(desc string, condition string, values []any) *Where {
	if desc != "" {
		return &Where{condition + desc, values}
	}

	return &Where{desc, values}
}

func genBindVariables(num int) string {
	vars := make([]string, 0, num)
	for i := 0; i < num; i++ {
		vars = append(vars, "?")
	}

	return strings.Join(vars, __SQL_COMMA)
}
