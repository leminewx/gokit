package statement

import (
	"testing"

	"gitee.com/leminewx/gokit/orm/statement/where"
)

func TestStatement_Insert(t *testing.T) {
	statement := NewStatement("user")
	statement.WithClause(CLAUSE_INSERT, "name", "age")
	sql, vars := statement.Build(CLAUSE_INSERT)
	t.Log(sql, vars)
}

func TestStatement_Select(t *testing.T) {
	statement := NewStatement("user")
	statement.WithClause(CLAUSE_SELECT, "name", "sex").
		WithClause(CLAUSE_WHERE, where.GT("a", 1), where.AND(where.LT("b", 3, 4)), where.IN("c", 1, 3, 5, 7), where.OR(where.BETWEEN("d", 1, 100)), where.LIKE("e", "%123%")).
		WithClause(CLAUSE_GROUPBY, "sex", "age").
		WithClause(CLAUSE_ORDERBY, "sex", ORDER_DESC, "age", "addr", "asc", "email").
		WithClause(CLAUSE_LIMIT, 3, 100)
	sql, vars := statement.Build(CLAUSE_SELECT, CLAUSE_FROM, CLAUSE_WHERE, CLAUSE_GROUPBY, CLAUSE_ORDERBY, CLAUSE_LIMIT)
	t.Log(sql, vars)
}

func TestStatement_Update(t *testing.T) {
	statement := NewStatement("user")
	statement.WithClause(CLAUSE_UPDATE, "age", 30, "email", "xxxx@qq.com").
		WithClause(CLAUSE_WHERE, where.EQ("name", "wx", ""), where.OR(where.EQ("sex", "man", "")))
	sql, vars := statement.Build(CLAUSE_UPDATE, CLAUSE_WHERE)
	t.Log(sql, vars)
}

func TestStatement_Delete(t *testing.T) {
	statement := NewStatement("user")
	statement.WithClause(CLAUSE_DELETE).
		WithClause(CLAUSE_WHERE, where.EQ("name", "wx", ""), where.OR(where.LT("age", 18)), where.IN("addr", "beijing", "chongqing", "sicuan"))
	sql, vars := statement.Build(CLAUSE_DELETE, CLAUSE_WHERE)
	t.Log(sql, vars)
}
