package statement

// ClauseType defines the dataype of SQL clause
type ClauseType int

const (
	CLAUSE_INSERT ClauseType = iota
	CLAUSE_VALUES
	CLAUSE_SELECT
	CLAUSE_DISTINCT
	CLAUSE_FROM
	CLAUSE_UPDATE
	CLAUSE_DELETE
	CLAUSE_COUNT
	CLAUSE_WHERE
	CLAUSE_GROUPBY
	CLAUSE_HAVING
	CLAUSE_ORDERBY
	CLAUSE_LIMIT
)
