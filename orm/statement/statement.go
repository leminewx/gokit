package statement

import (
	"strings"
)

// Statement defines the structure of SQL statement.
type Statement struct {
	tableName  string
	clauseSql  map[ClauseType]string
	clauseVars map[ClauseType][]any
}

func NewStatement(table string) *Statement {
	return &Statement{tableName: table}
}

func (own *Statement) WithTableName(name string) *Statement {
	own.tableName = name
	return own
}

// WithClause adds the type and variables of a specific clause.
func (own *Statement) WithClause(typ ClauseType, vars ...any) *Statement {
	if own.clauseSql == nil {
		own.clauseSql = make(map[ClauseType]string)
		own.clauseVars = make(map[ClauseType][]any)
	}

	own.clauseSql[typ], own.clauseVars[typ] = generators[typ](own.tableName, vars...)
	if typ == CLAUSE_SELECT {
		own.clauseSql[CLAUSE_FROM], _ = generators[CLAUSE_FROM](own.tableName)
	}
	return own
}

// Build generates the final SQL and SQLVars by the orders of SQL clauses.
func (own *Statement) Build(types ...ClauseType) (string, []any) {
	size := len(types)
	vars := make([]any, 0)
	sqls := make([]string, 0, size)
	for _, typ := range types {
		if sql, ok := own.clauseSql[typ]; ok {
			sqls = append(sqls, sql)
			vars = append(vars, own.clauseVars[typ]...)
		}
	}

	return strings.Join(sqls, " "), vars
}

// Reset initializes the parameters of the Statement.
func (own *Statement) Reset(table ...string) {
	if len(table) > 0 {
		own.tableName = table[0]
	}
	own.clauseSql = make(map[ClauseType]string)
	own.clauseVars = make(map[ClauseType][]any)
}
