package statement

import (
	"testing"

	"gitee.com/leminewx/gokit/orm/statement/where"
)

func TestInsert(t *testing.T) {
	t.Log(_insert("user", "name", "sex", "age"))
}

func TestValues(t *testing.T) {
	t.Log(_values("",
		[]any{"wx", 1, 30},
		[]any{"gh", 1, 25},
	))

	sqlInsert, valInsert := _insert("user", "name", "sex", "age")
	sqlValue, valValue := _values("",
		[]any{"wx", 1, 30},
		[]any{"gh", 1, 25},
	)
	t.Log(sqlInsert+" "+sqlValue, append(valInsert, valValue...))
}

func TestSelect(t *testing.T) {
	t.Log(_select("user"))
	t.Log(_select("user", "*"))
	t.Log(_select("user", "name", "sex", "age"))
}

func TestDistinct(t *testing.T) {
	t.Log(_distinct(""))
	t.Log(_distinct("", "name"))
	t.Log(_distinct("", "name", "sex"))

	sqlSelect, valSelect := _select("user", "name")
	sqlDistinct, valDistinct := _distinct("", "name", "sex", "age")
	t.Log(sqlSelect+" "+sqlDistinct, append(valSelect, valDistinct...))
}

func TestFrom(t *testing.T) {
	t.Log(_from(""))
	t.Log(_from("user"))
	t.Log(_from("user", "name", "sex"))

	sqlSelect, valSelect := _select("user", "name")
	sqlDistinct, valDistinct := _distinct("", "name", "sex", "age")
	sqlFrom, _ := _from("user")
	t.Log(sqlSelect+" "+sqlDistinct+" "+sqlFrom, append(valSelect, valDistinct...))
}

func TestWhere(t *testing.T) {
	t.Log(_where(""))
	t.Log(_where("", where.GT("a", 1), where.AND(where.LT("b", 3, 4)), where.IN("c", 1, 3, 5, 7), where.OR(where.BETWEEN("d", 1, 100)), where.LIKE("e", "%123%")))

	sqlSelect, valSelect := _select("user", "name")
	sqlDistinct, valDistinct := _distinct("", "name", "sex", "age")
	sqlFrom, _ := _from("user")
	sqlWhere, valWhere := _where("", where.EQ("sex", "man"), where.OR(where.LT("age", 30, 10)), where.IN("name", "wx", "gh", "dsc"))
	t.Log(sqlSelect+" "+sqlDistinct+" "+sqlFrom+" "+sqlWhere, append(append(valSelect, valDistinct...), valWhere...))
}

func BenchmarkWhere(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_where("", where.GT("a", 1), where.AND(where.LT("b", 3, 4)), where.IN("c", 1, 3, 5, 7), where.OR(where.BETWEEN("d", 1, 100)), where.LIKE("e", "%123%"))
	}
}

func TestUpdate(t *testing.T) {
	t.Log(_update("user", "c", nil, "d", "ddd", "a", "", "b", 1))
}

func BenchmarkUpdate(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_update("user", "c", nil, "d", "ddd", "a", "", "b", 1)
	}
}

func TestGroupBy(t *testing.T) {
	t.Log(_groupBy(""))
	t.Log(_groupBy("", "c"))
	t.Log(_groupBy("", "c", "d", "ddd", "a", "", "b"))
}

func TestOrderBy(t *testing.T) {
	t.Log(_orderBy(""))
	t.Log(_orderBy("", "c"))
	t.Log(_orderBy("", "d", ORDER_DESC))
	t.Log(_orderBy("", "c", "d", ORDER_ASC))
	t.Log(_orderBy("", "c", "d", ORDER_DESC, "ddd", "a", ORDER_ASC, "", "b"))
}

func TestLimit(t *testing.T) {
	t.Log(_limit(""))
	t.Log(_limit("", 10))
	t.Log(_limit("", 10, 10))
}

// func TestOrderBy(t *testing.T) {
// 	t.Log(_orderBy("name", "sex", "age"))
// }

// func TestUpdate(t *testing.T) {
// 	t.Log(_update("user", map[string]any{
// 		"name": "lx",
// 		"age":  31,
// 	}))
// }

// func TestDelte(t *testing.T) {
// 	t.Log(_delete("user"))
// }

// func TestCount(t *testing.T) {
// 	t.Log(_count("user"))
// }
