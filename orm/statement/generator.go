package statement

import (
	"fmt"
	"strings"

	"gitee.com/leminewx/gokit/orm/statement/where"
)

// generator generates a SQL clause and a clause variables by the input values
type generator func(table string, values ...any) (string, []any)

var generators map[ClauseType]generator

const __SQL_COMMA = ", "

func init() {
	generators = map[ClauseType]generator{
		CLAUSE_INSERT:   _insert,
		CLAUSE_VALUES:   _values,
		CLAUSE_SELECT:   _select,
		CLAUSE_DISTINCT: _distinct,
		CLAUSE_FROM:     _from,
		CLAUSE_UPDATE:   _update,
		CLAUSE_DELETE:   _delete,
		CLAUSE_COUNT:    _count,
		CLAUSE_WHERE:    _where,
		CLAUSE_GROUPBY:  _groupBy,
		CLAUSE_ORDERBY:  _orderBy,
		CLAUSE_LIMIT:    _limit,
	}
}

func _insert(table string, values ...any) (string, []any) {
	return fmt.Sprintf("INSERT INTO %s (%v)", table, genBindFieldNames(values...)), []any{}
}

func _values(_ string, values ...any) (string, []any) {
	var bindStr string
	var sqlVars []any
	var sqlBuilder strings.Builder

	num := len(values)
	sqlBuilder.WriteString("VALUES ")
	for i, value := range values {
		item := value.([]any)
		if bindStr == "" {
			bindStr = genBindVariables(len(item))
		}

		if sqlBuilder.WriteString(fmt.Sprintf("(%s)", bindStr)); i+1 < num {
			sqlBuilder.WriteString(__SQL_COMMA)
		}

		sqlVars = append(sqlVars, item...)
	}

	return sqlBuilder.String(), sqlVars
}

func _select(_ string, values ...any) (string, []any) {
	if len(values) == 0 {
		return "SELECT * ", []any{}
	}

	return "SELECT " + genBindFieldNames(values...), []any{}
}

func _distinct(_ string, values ...any) (string, []any) {
	size := len(values)
	if size == 0 {
		return "", []any{}
	}

	return "DISTINCT " + genBindFieldNames(values...), []any{}
}

func _from(table string, _ ...any) (string, []any) {
	return "FROM " + table, []any{}
}

func _where(_ string, values ...any) (string, []any) {
	var sqlVars []any
	var sqlBuilder strings.Builder
	if len(values) == 0 {
		return "", []any{}
	}

	first := true
	sqlBuilder.WriteString("WHERE ")
	for i := 0; i < len(values); i++ {
		where, ok := values[i].(*where.Where)
		if !ok || where.Desc == "" {
			continue
		}

		if !first {
			if !strings.Contains(where.Desc, "AND ") && !strings.Contains(where.Desc, "OR ") {
				sqlBuilder.WriteString(" AND ")
			} else {
				sqlBuilder.WriteByte(' ')
			}
		}

		sqlBuilder.WriteString(where.Desc)
		sqlVars = append(sqlVars, where.Args...)
		first = false
	}

	if orderBy := sqlBuilder.String(); orderBy != "" {
		return orderBy, sqlVars
	}

	return "", []any{}
}

func _update(table string, values ...any) (string, []any) {
	var sqlBuilder strings.Builder
	var sqlVars []any

	first := true
	for i := 0; i < len(values); i += 2 {
		if values[i+1] == nil {
			continue
		}

		if !first {
			sqlBuilder.WriteString("=?, ")
		}

		sqlBuilder.WriteString(values[i].(string))
		sqlVars = append(sqlVars, values[i+1])
		first = false
	}

	if len(sqlVars) > 0 {
		sqlBuilder.WriteString("=?")
	}

	return fmt.Sprintf("UPDATE %s SET %s", table, sqlBuilder.String()), sqlVars
}

func _delete(table string, _ ...any) (string, []any) {
	return "DELETE FROM " + table, []any{}
}

func _count(table string, _ ...any) (string, []any) {
	return "SELECT COUNT(*) FROM " + table, []any{}
}

func _groupBy(_ string, values ...any) (string, []any) {
	size := len(values)
	if size == 0 {
		return "", []any{}
	}

	return "GROUP BY " + genBindFieldNames(values...), []any{}
}

const (
	ORDER_ASC   string = "ASC"
	ORDER_DESC  string = "DESC"
	_ORDER_ASC  string = "asc"
	_ORDER_DESC string = "desc"
)

func _orderBy(_ string, values ...any) (string, []any) {
	if len(values) == 0 {
		return "", []any{}
	}

	var field string
	var sqlBuilder strings.Builder
	first := true
	sqlBuilder.WriteString("ORDER BY ")
	for i := 0; i < len(values); i++ {
		value, ok := values[i].(string)
		if !ok || value == "" {
			continue
		}

		if value == ORDER_ASC || value == ORDER_DESC || value == _ORDER_ASC || value == _ORDER_DESC {
			if field == "" {
				continue
			}

			if !first {
				sqlBuilder.WriteString(__SQL_COMMA)
			}

			value = strings.ToUpper(value)
			sqlBuilder.WriteString(field)
			sqlBuilder.WriteByte(' ')
			sqlBuilder.WriteString(string(value))
			first = false
			field = ""
			continue
		}

		if field == "" {
			field = value
			continue
		}

		if !first {
			sqlBuilder.WriteString(__SQL_COMMA)
		}

		sqlBuilder.WriteString(field)
		first = false
		field = value
	}

	if field != "" {
		if !first {
			sqlBuilder.WriteString(__SQL_COMMA)
		}
		sqlBuilder.WriteString(field)
	}

	return sqlBuilder.String(), []any{}
}

func _limit(_ string, values ...any) (string, []any) {
	switch len(values) {
	case 0:
		return "", []any{}
	case 1:
		return "LIMIT ?", values
	default:
		return "LIMIT ?, ?", values[:2]
	}
}

func genBindVariables(num int) string {
	vars := make([]string, 0, num)
	for i := 0; i < num; i++ {
		vars = append(vars, "?")
	}

	return strings.Join(vars, __SQL_COMMA)
}

func genBindFieldNames(fields ...any) string {
	items := make([]string, 0, len(fields))
	for _, field := range fields {
		f, ok := field.(string)
		if !ok || f == "" {
			continue
		}
		items = append(items, f)
	}

	return strings.Join(items, __SQL_COMMA)
}
