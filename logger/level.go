package logger

type Level int8

const (
	L_DEBUG Level = iota + 1
	L_INFO
	L_WARN
	L_ERROR
	L_FATAL
)

func (own Level) String() string {
	switch own {
	case L_DEBUG:
		return "Debug"
	case L_INFO:
		return "Info"
	case L_WARN:
		return "Warn"
	case L_ERROR:
		return "Error"
	case L_FATAL:
		return "Fatal"
	default:
		return ""
	}
}

const (
	_STR_DEBUG = "| Debug | "
	_STR_INFO  = "| Info  | "
	_STR_WARN  = "| Warn  | "
	_STR_ERROR = "| Error | "
	_STR_FATAL = "| Fatal | "
)

func (own Level) GetString() string {
	switch own {
	case L_DEBUG:
		return _STR_DEBUG
	case L_INFO:
		return _STR_INFO
	case L_WARN:
		return _STR_WARN
	case L_ERROR:
		return _STR_ERROR
	case L_FATAL:
		return _STR_FATAL
	default:
		return ""
	}
}

const (
	_STR_DEBUG_WITH_COLOR = "|\033[47m\033[30m Debug \033[0m|" // Cyan
	_STR_INFO_WITH_COLOR  = "|\033[42m\033[30m Info  \033[0m|" // Green
	_STR_WARN_WITH_COLOR  = "|\033[43m\033[30m Warn  \033[0m|" // Yellow
	_STR_ERROR_WITH_COLOR = "|\033[41m\033[30m Error \033[0m|" // Red
	_STR_FATAL_WITH_COLOR = "|\033[45m\033[30m Fatal \033[0m|" // Purple
)

func (own Level) GetColorString() string {
	switch own {
	case L_DEBUG:
		return _STR_DEBUG_WITH_COLOR
	case L_INFO:
		return _STR_INFO_WITH_COLOR
	case L_WARN:
		return _STR_WARN_WITH_COLOR
	case L_ERROR:
		return _STR_ERROR_WITH_COLOR
	case L_FATAL:
		return _STR_FATAL_WITH_COLOR
	default:
		return ""
	}
}
