package logger

import (
	"testing"
)

func TestLogger(t *testing.T) {
	logger := New().WithLevel(L_INFO)
	defer logger.Close()

	logger.Debug("this is a debug message")
	logger.Debugf("this is a %s message", "DEBUG")
	logger.Info("this is a debug message")
	logger.Infof("this is a %s message", "INFO")
	logger.Warn("this is a debug message")
	logger.Warnf("this is a %s message", "WARN")
	logger.Error("this is a error message")
	logger.Errorf("this is a %s message", "ERROR")
	logger.Fatal("this is a fatal message")
	logger.Fatalf("this is a %s message\n\n", "FATAL")
}

func TestLogger_WithFullFilename(t *testing.T) {
	logger := New().WithFullFilename()
	logger.SetLogFile(L_ERROR, "../error.log")

	logger.Debug("this is a debug message")
	logger.Debugf("this is a %s message", "DEBUG")
	logger.Info("this is a debug message")
	logger.Infof("this is a %s message", "INFO")
	logger.Warn("this is a debug message")
	logger.Warnf("this is a %s message", "WARN")
	logger.Error("this is a error message")
	logger.Errorf("this is a %s message", "ERROR")
	logger.Fatal("this is a fatal message")
	logger.Fatalf("this is a %s message", "FATAL")
}

func BenchmarkJoinString(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = "12345" + "!@#$^" + "qwert"
	}
}
