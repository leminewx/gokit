package logger

var (
	defaultLogger = New()
)

var (
	Debug  = defaultLogger.Debug
	Debugf = defaultLogger.Debugf
	Info   = defaultLogger.Info
	Infof  = defaultLogger.Infof
	Warn   = defaultLogger.Warn
	Warnf  = defaultLogger.Warnf
	Error  = defaultLogger.Error
	Errorf = defaultLogger.Errorf
	Fatal  = defaultLogger.Fatal
	Fatalf = defaultLogger.Fatalf
)

func WithLevel(level Level) *Logger {
	return defaultLogger.WithLevel(level)
}

func WithFullFilename() *Logger {
	return defaultLogger.WithFullFilename()
}
