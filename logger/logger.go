package logger

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"time"
)

const _TIME_FORMAT = "2006-01-02 15:04:05.000"

type Logger struct {
	fullFilename bool
	printLevel   Level
	printer      io.Writer
	writeLevel   Level
	writer       *os.File
	builderPool  sync.Pool
	lock         sync.Mutex
}

func New() *Logger {
	return &Logger{
		printLevel:  L_DEBUG,
		printer:     os.Stdout,
		builderPool: sync.Pool{New: func() any { return &strings.Builder{} }},
	}
}

func (own *Logger) WithLevel(level Level) *Logger {
	own.lock.Lock()
	own.printLevel = level
	own.lock.Unlock()
	return own
}

func (own *Logger) WithFullFilename() *Logger {
	own.lock.Lock()
	own.fullFilename = true
	own.lock.Unlock()
	return own
}

func (own *Logger) SetLogFile(level Level, filename string) error {
	own.lock.Lock()
	defer own.lock.Unlock()
	file, err := os.OpenFile(filename, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}

	own.writeLevel = level
	own.writer = file
	return nil
}

func (own *Logger) Debug(msgs ...string) {
	own.log(L_DEBUG, 3, msgs...)
}

func (own *Logger) Info(msgs ...string) {
	own.log(L_INFO, 3, msgs...)
}

func (own *Logger) Warn(msgs ...string) {
	own.log(L_WARN, 3, msgs...)
}

func (own *Logger) Error(msgs ...string) {
	own.log(L_ERROR, 3, msgs...)
}

func (own *Logger) Fatal(msgs ...string) {
	own.log(L_FATAL, 3, msgs...)
	own.Close()
	os.Exit(1)
}

func (own *Logger) log(level Level, skip int, msgs ...string) {
	if level < own.printLevel && level < own.writeLevel {
		return
	}

	// format the head and the body of a log info
	printLevel, writeLevel, builder := own.formatHeader(level, skip)
	body := own.joinMessages(builder, msgs...)

	// write log info to the builder
	own.lock.Lock()
	if printLevel != "" {
		fmt.Fprintln(own.printer, printLevel, body)
	}
	if writeLevel != "" {
		fmt.Fprintln(own.writer, writeLevel, body)
	}
	own.lock.Unlock()
}

func (own *Logger) Debugf(fmt string, args ...any) {
	own.logf(L_DEBUG, 3, fmt, args...)
}

func (own *Logger) Infof(fmt string, args ...any) {
	own.logf(L_INFO, 3, fmt, args...)
}

func (own *Logger) Warnf(fmt string, args ...any) {
	own.logf(L_WARN, 3, fmt, args...)
}

func (own *Logger) Errorf(fmt string, args ...any) {
	own.logf(L_ERROR, 3, fmt, args...)
}

func (own *Logger) Fatalf(fmt string, args ...any) {
	own.logf(L_FATAL, 3, fmt, args...)
	os.Exit(1)
}

func (own *Logger) logf(level Level, skip int, format string, args ...any) {
	if level < own.printLevel && level < own.writeLevel {
		return
	}

	// format the head and the body of a log info
	printLevel, writeLevel, builder := own.formatHeader(level, skip)
	body := own.formatMessages(builder, format, args...)

	// write log info to the builder
	own.lock.Lock()
	if printLevel != "" {
		fmt.Fprintln(own.printer, printLevel, body)
	}
	if writeLevel != "" {
		fmt.Fprintln(own.writer, writeLevel, body)
	}
	own.lock.Unlock()
}

func (own *Logger) formatHeader(level Level, skip int) (printLevel, writeLevel string, builder *strings.Builder) {
	// format the levels info
	if level >= own.printLevel {
		printLevel = level.GetColorString()
	}
	if level >= own.writeLevel {
		writeLevel = level.GetString()
	}

	// format the datetime info
	builder = own.builderPool.Get().(*strings.Builder)
	builder.WriteString(time.Now().Format(_TIME_FORMAT))
	builder.WriteByte(' ')

	// format the caller info
	if _, file, line, ok := runtime.Caller(skip); ok {
		if own.fullFilename {
			builder.WriteString(file)
		} else {
			builder.WriteString(filepath.Base(file))
		}

		builder.WriteByte(':')
		builder.WriteString(strconv.Itoa(line))
		builder.WriteString(": ")
	}

	return printLevel, writeLevel, builder
}

func (own *Logger) joinMessages(builder *strings.Builder, msgs ...string) string {
	for _, msg := range msgs {
		builder.WriteString(msg)
	}

	body := builder.String()
	builder.Reset()
	own.builderPool.Put(builder)
	return body
}

func (own *Logger) formatMessages(builder *strings.Builder, format string, args ...any) string {
	if len(args) == 0 {
		builder.WriteString(format)
	} else {
		fmt.Fprintf(builder, format, args...)
	}

	body := builder.String()
	builder.Reset()
	own.builderPool.Put(builder)
	return body
}

func (own *Logger) Close() (err error) {
	own.lock.Lock()
	if own.writer != nil {
		err = own.writer.Close()
		own.writer = nil
	}

	own.lock.Unlock()
	return
}
