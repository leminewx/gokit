package stream

import (
	"strconv"
	"testing"
)

type testItem struct {
	key   any
	group any
}

func (own *testItem) WithKey(key any) Item {
	own.key = key
	return own
}

func (own *testItem) WithGroup(group any) Item {
	own.group = group
	return own
}

func (own *testItem) GetKey() any { return own.key }

func (own *testItem) GetGroup() any { return own.group }

func TestStream_Group(t *testing.T) {
	for item := range NewOf(
		&testItem{key: "1", group: "a"},
		&testItem{key: "2", group: "b"},
		&testItem{key: "3", group: "c"},
		&testItem{key: "4", group: "a"},
		&testItem{key: "5", group: "b"},
		&testItem{key: "6", group: "c"},
		&testItem{key: "7", group: "a"},
	).Group(func(item Item) any {
		if item.GetGroup() == "a" {
			return "d"
		}
		return "f"
	}).Channel() {
		t.Log(item)
	}
}

func TestStream_Distinct(t *testing.T) {
	for item := range NewOf(
		&testItem{key: "1", group: "a"},
		&testItem{key: "2", group: "b"},
		&testItem{key: "3", group: "c"},
		&testItem{key: "4", group: "a"},
		&testItem{key: "5", group: "b"},
		&testItem{key: "6", group: "c"},
		&testItem{key: "6", group: "a"},
	).Distinct(func(item Item) any {
		key := item.GetKey()
		if key == "2" || key == "4" || key == "6" {
			return "2"
		}
		return key
	}).Channel() {
		t.Log(item)
	}
}

func TestStream_Filter(t *testing.T) {
	for item := range NewOf(
		&testItem{key: "1", group: "a"},
		&testItem{key: "2", group: "b"},
		&testItem{key: "3", group: "c"},
		&testItem{key: "4", group: "a"},
		&testItem{key: "5", group: "b"},
		&testItem{key: "6", group: "c"},
		&testItem{key: "6", group: "a"},
	).Filter(func(item Item) bool {
		return item.GetKey() != "3"
	}).Channel() {
		t.Log(item)
	}
}

func TestStream_Concat(t *testing.T) {
	for item := range NewOf(
		&testItem{key: "1", group: "aa"},
		&testItem{key: "2", group: "b"},
		&testItem{key: "3", group: "c"},
		&testItem{key: "4", group: "aa"},
		&testItem{key: "5", group: "b"},
		&testItem{key: "6", group: "c"},
		&testItem{key: "6", group: "ac"},
	).Concat(NewFrom(func() Stream {
		source := make(chan Item, 1)
		go func() {
			for i := 0; i < 10; i++ {
				source <- &testItem{strconv.Itoa(i), "aa"}
			}
			close(source)
		}()

		return &stream{source}
	}), NewOf(
		&testItem{key: "11", group: "aa"},
		&testItem{key: "22", group: "bb"},
		&testItem{key: "33", group: "cc"},
		&testItem{key: "44", group: "dd"},
		&testItem{key: "55", group: "ee"},
		&testItem{key: "66", group: "aa"},
		&testItem{key: "66", group: "ee"},
	)).Filter(func(item Item) bool {
		return item.GetGroup() != "aa"
	}).Distinct(nil).Channel() {
		t.Log(item)
	}
}
