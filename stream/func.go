package stream

type (
	KeyFunc    func(item Item) any
	FilterFunc func(item Item) bool
)
