package stream

type (
	Item interface {
		WithKey(key any) Item
		WithGroup(group any) Item

		GetKey() any
		GetGroup() any
	}

	TimeSeriesItem interface {
		Item

		GetDateime() string
		GetTimestamp() int64
	}
)
