package stream

import (
	"sync"
)

type (
	GenerateFunc func() Stream

	Stream interface {
		Group(keyFunc KeyFunc) Stream
		Distinct(keyFunc KeyFunc) Stream
		Filter(filterFunc FilterFunc) Stream
		Concat(others ...Stream) Stream
		// Head(num int) Stream
		// Tail(num int) Stream

		Channel() <-chan Item
	}

	stream struct {
		source <-chan Item
	}
)

// NewBy create a Stream by a input channel
func NewBy(source <-chan Item) Stream {
	return &stream{source}
}

// NewOf create a Stream by some items
func NewOf(items ...Item) Stream {
	source := make(chan Item, 1)
	go func() {
		for _, item := range items {
			source <- item
		}
		close(source)
	}()

	return &stream{source}
}

// NewWith create a Stream with some streams
func NewWith(streams ...Stream) Stream {
	if len(streams) == 0 {
		return nil
	}

	return streams[0].Concat(streams[1:]...)
}

// NewFrom create a Stream by a generate func
func NewFrom(generate GenerateFunc) Stream {
	return generate()
}

func (own *stream) Group(keyFunc KeyFunc) Stream {
	source := make(chan Item, 1)
	go func() {
		for item := range own.source {
			source <- item.WithGroup(keyFunc(item))
		}
		close(source)
	}()

	return &stream{source}
}

func (own *stream) Distinct(keyFunc KeyFunc) Stream {
	source := make(chan Item, 1)
	go func() {
		keys := make(map[any]struct{})
		for item := range own.source {
			var key any
			if keyFunc != nil {
				key = keyFunc(item)
			} else {
				key = item.GetKey()
			}

			if _, ok := keys[key]; !ok {
				keys[key] = struct{}{}
				source <- item
			}
		}
		close(source)
	}()

	return &stream{source}
}

func (own *stream) Filter(filterFunc FilterFunc) Stream {
	source := make(chan Item, 1)
	go func() {
		for item := range own.source {
			if !filterFunc(item) {
				source <- item
			}
		}
		close(source)
	}()

	return &stream{source}
}

func (own *stream) Concat(others ...Stream) Stream {
	source := make(chan Item, 1)
	go func() {
		var wg sync.WaitGroup
		streams := append(append(make([]Stream, 0, len(others)+1), own), others...)
		for _, stream := range streams {
			if stream != nil {
				wg.Add(1)
				s := stream.Channel()
				go func(wg *sync.WaitGroup) {
					for item := range s {
						source <- item
					}
					wg.Done()
				}(&wg)
			}
		}

		wg.Wait()
		close(source)
	}()

	return &stream{source}
}

func (own *stream) Channel() <-chan Item {
	return own.source
}
