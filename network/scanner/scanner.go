package scanner

import (
	"fmt"
	"log/slog"
	"net"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	"gitee.com/leminewx/gokit/network/protocol/icmp"
)

// Scanner defines the structure of the network scanner
type Scanner struct {
	isClosed    int32 // the status of this network scanner, 0-closed;1-opened;2-closing
	timeout     int64
	interval    int64
	concurrency int64
	closeChan   chan struct{}

	lock sync.RWMutex
}

const (
	DEFAULT_SCANN_TIMEOUT     = 10  // default scan timeout
	DEFAULT_SCANN_CONCURRENCY = 255 // default scan concurrency
)

// New creates a network scanner
func New() *Scanner {
	return &Scanner{
		timeout:     DEFAULT_SCANN_TIMEOUT,
		concurrency: DEFAULT_SCANN_CONCURRENCY,
		closeChan:   make(chan struct{}, 1),
	}
}

func (own *Scanner) WithConcurrency(concurrency int64) *Scanner {
	if concurrency < 1 {
		concurrency = DEFAULT_SCANN_CONCURRENCY
	}

	own.lock.Lock()
	own.concurrency = concurrency
	own.lock.Unlock()
	return own
}

func (own *Scanner) WithTimeout(timeout int64) *Scanner {
	if timeout < 1 {
		timeout = DEFAULT_SCANN_TIMEOUT
	}

	own.lock.Lock()
	own.timeout = timeout
	own.lock.Unlock()
	return own
}

// WithInterval sets the loop interval of scanning, and default not loop
func (own *Scanner) WithInterval(interval int64) *Scanner {
	if interval < 10 {
		interval = 10
	} else if interval > 3600 {
		interval = 3600
	}

	own.lock.Lock()
	own.interval = interval
	own.lock.Unlock()
	return own
}

func (own *Scanner) ScannIPV4(gateways ...string) (<-chan string, error) {
	return own.scannIPV4(0, gateways)
}

func (own *Scanner) ScannIPV4Port(port int, gateways ...string) (<-chan string, error) {
	if port < 1 || 65535 < port {
		return nil, fmt.Errorf("invalid port: %d", port)
	}

	return own.scannIPV4(port, gateways)
}

func (own *Scanner) scannIPV4(port int, gateways []string) (<-chan string, error) {
	if !atomic.CompareAndSwapInt32(&own.isClosed, 0, 1) {
		return nil, fmt.Errorf("scanner is scanning or stopping")
	}

	return own.scann(port, gateways...), nil
}

func (own *Scanner) scann(port int, gateways ...string) <-chan string {
	var wg sync.WaitGroup
	addrChan := own.generateAddrChan(gateways...)
	allowChan := own.getAllowChan()
	addrOkChan := make(chan string, 30)
	slog.Info("[Gokit] network scanner is scanning...")

	go func() {
	EXIST:
		for range allowChan {
			select {
			case <-own.closeChan:
				slog.Info("[Gokit] network scanner stoped scanning and waiting for the end")
				go func() {
					for range addrChan {
					}
				}()
				break EXIST
			case addr, ok := <-addrChan:
				if !ok {
					slog.Info("[Gokit] network scanner is completed scanning and waiting for the end")
					atomic.CompareAndSwapInt32(&own.isClosed, 1, 2)
					break EXIST
				} else if len(addr) == 0 {
					continue
				}

				wg.Add(1)
				go func(_wg *sync.WaitGroup) {
					var okAddr string
					if port > 0 {
						okAddr = own.checkIPV4Port(addr, port)
					} else {
						okAddr = own.checkIPV4Network(addr)
					}

					if okAddr != "" {
						addrOkChan <- okAddr
					}

					_wg.Done()
					allowChan <- struct{}{}
				}(&wg)
			}
		}

		wg.Wait()
		close(allowChan)
		close(addrOkChan)
		atomic.CompareAndSwapInt32(&own.isClosed, 2, 0)
		slog.Info("[Gokit] network scanner is stoped")
	}()

	return addrOkChan
}

// Stop stops the current sanning
func (own *Scanner) Stop() error {
	if !atomic.CompareAndSwapInt32(&own.isClosed, 1, 2) {
		return fmt.Errorf("scanner is stoped or stopping")
	}

	own.lock.Lock()
	own.interval = 0
	own.closeChan <- struct{}{}
	own.lock.Unlock()
	return nil
}

func (own *Scanner) generateAddrChan(gateways ...string) <-chan string {
	addrChan := make(chan string, 255)
	go func() {
		if atomic.CompareAndSwapInt64(&own.interval, 0, 0) {
			own.generateAddrs(addrChan, gateways...)
			close(addrChan)
			return
		}

		ticker := time.NewTicker(time.Duration(own.interval) * time.Second)
		for range ticker.C {
			if own.generateAddrs(addrChan, gateways...); atomic.CompareAndSwapInt64(&own.interval, 0, 0) {
				close(addrChan)
				return
			}
			ticker.Reset(time.Duration(own.interval) * time.Second)
		}
	}()

	return addrChan
}

func (own *Scanner) generateAddrs(addrChan chan string, gateways ...string) {
	for _, gateway := range gateways {
		if ip := net.ParseIP(gateway); ip == nil {
			continue
		}

		for _, addr := range GenerateIPV4FullNetworkSegmentAddresses(gateway) {
			addrChan <- addr
		}
	}
}

func (own *Scanner) getAllowChan() chan struct{} {
	allowChan := make(chan struct{}, own.concurrency)
	for i := int64(0); i < own.concurrency; i++ {
		allowChan <- struct{}{}
	}

	return allowChan
}

func (own *Scanner) checkIPV4Network(ip string) string {
	if fail, err := icmp.Ping(ip, 5); err != nil || fail > 0.1 {
		return ""
	}

	return ip
}

func (own *Scanner) checkIPV4Port(ip string, port int) string {
	if conn, err := net.DialTimeout("tcp", fmt.Sprintf("%s:%d", ip, port), time.Duration(own.timeout)*time.Second); err == nil {
		conn.Close()
		return ip
	}

	return ""
}

func GenerateIPV4FullNetworkSegmentAddresses(ipv4Gateway string) []string {
	parts := strings.Split(ipv4Gateway, ".")
	prefix := strings.Join(parts[:3], ".")
	addrs := make([]string, 0, 255)
	for i := 1; i < 256; i++ {
		addrs = append(addrs, fmt.Sprintf("%s.%d", prefix, i))
	}

	return addrs
}

func Collect[T any](channel <-chan T) []T {
	res := make([]T, 0)
	for val := range channel {
		res = append(res, val)
	}
	return res
}
