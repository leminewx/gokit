package scanner

import (
	"testing"
	"time"
)

func TestScanner_ScannNetwork(t *testing.T) {
	scanner := New()
	ips, err := scanner.ScannIPV4Port(20300, "172.22.67.1")
	if err != nil {
		t.Fatal(err)
	}

	// [172.22.67.2 172.22.67.8 172.22.67.170 172.22.67.110 172.22.67.83 172.22.67.111 172.22.67.86 172.22.67.87 172.22.67.191 172.22.67.192 172.22.67.117 172.22.67.119 172.22.67.194]
	// [172.22.67.2 172.22.67.8 172.22.67.119 172.22.67.194 172.22.67.110 172.22.67.111 172.22.67.191 172.22.67.199 172.22.67.170 172.22.67.117 172.22.67.192 172.22.67.83]
	t.Log(Collect(ips))
}

func TestScanner_ScannPort(t *testing.T) {
	scanner := New().WithConcurrency(1000)
	ips, err := scanner.ScannIPV4Port(20300, "10.254.199.27", "172.62.22.170", "10.254.193.1", "10.254.195.1", "10.254.197.1", "10.254.199.1", "10.254.216.1")
	if err != nil {
		t.Fatal(err)
	}

	t.Log(Collect(ips))
}

func TestScanner_Stop(t *testing.T) {
	scanner := New()
	go func() {
		time.Sleep(20 * time.Second)
		if err := scanner.Stop(); err != nil {
			panic(err)
		}
	}()

	ips, err := scanner.ScannIPV4Port(20300, "10.254.199.27", "172.62.22.170", "10.254.193.1", "10.254.195.1", "10.254.197.1", "10.254.199.1", "10.254.216.1")
	if err != nil {
		t.Fatal(err)
	}

	t.Log(Collect(ips))
}

func TestScanner_LoopScannPort(t *testing.T) {
	scanner := New().WithConcurrency(1000).WithInterval(20)
	go func() {
		time.Sleep(60 * time.Second)
		if err := scanner.Stop(); err != nil {
			panic(err)
		}
	}()

	ips, err := scanner.ScannIPV4Port(20300, "10.254.199.27", "172.62.22.170", "10.254.193.1", "10.254.195.1", "10.254.197.1", "10.254.199.1", "10.254.216.1")
	if err != nil {
		t.Fatal(err)
	}

	t.Log(Collect(ips))
}
