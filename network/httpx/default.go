package httpx

import (
	"bytes"
	"sync"
)

var (
	maxMultipartMemory int64 = 10 << 20

	lock sync.RWMutex

	bufferPool = sync.Pool{
		New: func() interface{} {
			return bytes.NewBuffer(make([]byte, 4096))
		},
	}
)

func SetMaxMultipartMemory(max int64) {
	lock.Lock()
	maxMultipartMemory = max
	lock.Unlock()
}

func GetBuffer() *bytes.Buffer {
	return bufferPool.Get().(*bytes.Buffer)
}

func PutBuffer(buf *bytes.Buffer) {
	buf.Reset()
	bufferPool.Put(buf)
}
