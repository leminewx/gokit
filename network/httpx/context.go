package httpx

import (
	"bytes"
	"context"
	"crypto/sha1"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"mime"
	"net"
	"net/http"
	"net/url"
	"os"
	"strings"
	"text/template"
)

type Context struct {
	StatusCode int

	query    url.Values
	request  *http.Request
	response http.ResponseWriter
}

func NewContext(resp http.ResponseWriter, req *http.Request) *Context {
	if content, _, err := mime.ParseMediaType(req.Header.Get("Content-Type")); err == nil {
		if content == "application/x-www-form-urlencoded" {
			req.ParseForm()
		} else if content == "multipart/form-data" {
			req.ParseMultipartForm(maxMultipartMemory)
		}
	}

	return &Context{
		query:    req.URL.Query(),
		request:  req,
		response: resp,
	}
}

func (own *Context) GetURL() *url.URL { return own.request.URL }

func (own *Context) GetMethod() string { return own.request.Method }

func (own *Context) GetPathValue(name string) string { return own.request.PathValue(name) }

func (own *Context) GetFormValue(key string) string { return own.request.Form.Get(key) }

func (own *Context) GetQueryValue(key string) string { return own.query.Get(key) }

func (own *Context) GetPostFormValue(key string) string {
	if own.request.PostForm == nil {
		return ""
	}

	return own.request.PostForm.Get(key)
}

func (own *Context) GetMultipartFormValue(key string) string {
	if own.request.MultipartForm == nil {
		return ""
	}

	if params := own.request.MultipartForm.Value[key]; len(params) > 0 {
		return params[0]
	}

	return ""
}

func (own *Context) GetHeader(key string) string {
	return own.request.Header.Get(key)
}

func (own *Context) ReadJson(data any) error {
	buf := GetBuffer()
	if _, err := io.Copy(buf, own.request.Body); err != nil {
		return err
	}

	err := json.Unmarshal(buf.Bytes(), data)
	PutBuffer(buf)
	return err
}

func (own *Context) GetRequest() *http.Request { return own.request }

func (own *Context) GetResponse() http.ResponseWriter { return own.response }

func (own *Context) WithHeader(key, value string) *Context {
	own.response.Header().Set(key, value)
	return own
}

func (own *Context) WithContext(ctx context.Context) *Context {
	own.request = own.request.WithContext(ctx)
	return own
}

func (own *Context) ResponseSSE(sse func() <-chan []byte) {
	own.WithHeader("Connection", "keep-alive").
		WithHeader("Cache-Control", "no-cache").
		WithHeader("Content-Type", "text/event-stream")

	var buffer bytes.Buffer
	for data := range sse() {
		buffer.Write([]byte("data: "))
		buffer.Write(data)
		buffer.Write([]byte("\n\n"))
		own.response.Write(buffer.Bytes())
		own.response.(http.Flusher).Flush()
		buffer.Reset()
	}
}

func (own *Context) ResponseJSON(code int, data any) {
	dataBytes, err := json.Marshal(&data)
	if err != nil {
		http.Error(own.response, fmt.Sprintf("Failed to marshal the json data: %v", err), http.StatusInternalServerError)
		return
	}

	own.StatusCode = code
	own.response.WriteHeader(code)
	if _, err = own.response.Write(dataBytes); err != nil {
		http.Error(own.response, fmt.Sprintf("Failed to write the json data to the response: %v", err), http.StatusInternalServerError)
	}
}

func (own *Context) ResponseFile(filename string) {
	file, err := os.Open(filename)
	if err != nil {
		http.Error(own.response, fmt.Sprintf("Failed to open the file: %v", err), http.StatusNotFound)
		return
	}
	defer file.Close()

	info, err := file.Stat()
	if err != nil {
		http.Error(own.response, fmt.Sprintf("Failed to check the file status: %v", err), http.StatusInternalServerError)
		return
	}

	own.WithHeader("Content-Type", http.DetectContentType([]byte(info.Name()))).
		WithHeader("Content-Length", fmt.Sprintf("%d", info.Size()))

	if _, err = io.Copy(own.response, file); err != nil {
		http.Error(own.response, "Failed to write the file data to the response", http.StatusInternalServerError)
	}
}

var templates = make(map[string]*template.Template)

func (own *Context) ResponseHTML(filename, tmplname string, data any) {
	tmpl, ok := templates[filename]
	if !ok {
		tmpl = template.Must(template.ParseFiles(filename))
		templates[filename] = tmpl
	}

	var err error
	if tmplname == "" {
		err = tmpl.Execute(own.response, &data)
	} else {
		err = tmpl.ExecuteTemplate(own.response, tmplname, &data)
	}

	if err != nil {
		http.Error(own.response, fmt.Sprintf("Failed to execute the HTML to the response: %v", err), http.StatusInternalServerError)
	}
}

func (own *Context) ResponseUpgrade(upgrade func(conn net.Conn)) {
	header := own.request.Header
	if !strings.Contains(header.Get("Connection"), "upgrade") || !strings.EqualFold(header.Get("Upgrade"), "websocket") {
		http.Error(own.response, "Failed to upgrade connection to WebSocket", http.StatusBadRequest)
	}

	// set the header of WebSocket
	own.WithHeader("Upgrade", "websocket").
		WithHeader("Connection", "Upgrade").
		WithHeader("Sec-WebSocket-Accept", computeAcceptKey(header.Get("Sec-WebSocket-Key")))

	own.response.WriteHeader(http.StatusSwitchingProtocols)

	// get the HiJacker from the response
	hijacker, ok := own.response.(http.Hijacker)
	if !ok {
		http.Error(own.response, "Not supported HiJacker", http.StatusBadRequest)
		return
	}

	// get the tcp connection from the HiJacker
	conn, _, err := hijacker.Hijack()
	if err != nil {
		http.Error(own.response, fmt.Sprintf("Failed to get the HiJacker connection: %v", err), http.StatusInternalServerError)
		return
	}

	upgrade(conn)
	conn.Close()
}

func computeAcceptKey(key string) string {
	hash := sha1.New()
	io.WriteString(hash, key+"258EAFA5-E914-47DA-95CA-C5AB0DC85B11")
	return base64.StdEncoding.EncodeToString(hash.Sum(nil))
}
