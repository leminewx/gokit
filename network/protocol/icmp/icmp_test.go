package icmp

import "testing"

func TestPing(t *testing.T) {
	loss, err := Ping("10.254.249.123", 5)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(loss)
}
