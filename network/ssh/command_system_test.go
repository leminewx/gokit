package ssh

import (
	"testing"
)

func TestPidof_Exist(t *testing.T) {
	client := NewClient(_TEST_DEVICE_IP, "root", "Cictci@2022")
	if err := client.Connect(); err != nil {
		t.Fatal(err)
	}
	defer client.Close()

	pids, err := client.Pidof("xds")
	if err != nil {
		t.Fatal(err)
	}
	t.Log(pids)
}

func TestPidof_NotExist(t *testing.T) {
	client := NewClient(_TEST_DEVICE_IP, "root", "Cictci@2022")
	if err := client.Connect(); err != nil {
		t.Fatal(err)
	}
	defer client.Close()

	if _, err := client.Pidof("12345 fasdf"); err != nil {
		t.Fatal(err)
	}
}

func TestPidof_ErrorFlag(t *testing.T) {
	client := NewClient(_TEST_DEVICE_IP, "root", "Cictci@2022")
	if err := client.Connect(); err != nil {
		t.Fatal(err)
	}
	defer client.Close()

	if _, err := client.Pidof("12345 -12345"); err != nil {
		t.Fatal(err)
	}
}

func TestKill_Exist(t *testing.T) {
	client := NewClient(_TEST_DEVICE_IP, "root", "Cictci@2022")
	if err := client.Connect(); err != nil {
		t.Fatal(err)
	}
	defer client.Close()

	if err := client.Kill("1019"); err != nil {
		t.Fatal(err)
	}
}

func TestKill_NotExist(t *testing.T) {
	client := NewClient(_TEST_DEVICE_IP, "root", "Cictci@2022")
	if err := client.Connect(); err != nil {
		t.Fatal(err)
	}
	defer client.Close()

	if err := client.Kill("666666"); err != nil {
		t.Fatal(err)
	}
}

func TestKill_ErrorFlag(t *testing.T) {
	client := NewClient(_TEST_DEVICE_IP, "root", "Cictci@2022")
	if err := client.Connect(); err != nil {
		t.Fatal(err)
	}
	defer client.Close()

	if err := client.Kill("-4 666666"); err != nil {
		t.Fatal(err)
	}
}

func TestKillall(t *testing.T) {
	client := NewClient(_TEST_DEVICE_IP, "root", "Cictci@2022")
	if err := client.Connect(); err != nil {
		t.Fatal(err)
	}
	defer client.Close()

	if err := client.Killall("omCore"); err != nil {
		t.Fatal(err)
	}

	if err := client.Killall("omCore"); err != nil {
		t.Fatal(err)
	}
}
