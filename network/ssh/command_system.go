package ssh

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"time"
)

var pidofRegexp = regexp.MustCompile(`\d+`)

func (own *Client) Pidof(names ...string) ([]string, error) {
	res, err := own.Execute("pidof " + strings.Join(names, " "))
	if err != nil {
		return nil, err
	}

	return pidofRegexp.FindAllString(res, -1), nil
}

func (own *Client) Kill(pids ...string) error {
	if _, err := own.Execute("kill -9 " + strings.Join(pids, " ")); err != nil {
		return err
	}

	return own.IsExistPids(pids...)
}

func (own *Client) Killall(names ...string) error {
	processes := strings.Join(names, " ")
	if _, err := own.Execute("killall -9 " + processes); err != nil {
		return err
	}

	pids, err := own.Pidof(processes)
	if err != nil {
		return err
	} else if len(pids) > 0 {
		return fmt.Errorf("[SSH] some processes are killed, but their processes are running: %v", pids)
	}

	return nil
}

func (own *Client) IsExistPids(pids ...string) error {
	if resp, err := own.Execute("kill -0 " + strings.Join(pids, " ")); err != nil {
		return err
	} else if resp != "" {
		return fmt.Errorf(resp)
	}

	return nil
}

func (own *Client) WaitProcesses(tiemout int64, names ...string) (pids []string, err error) {
	for i := int64(0); i < tiemout; i++ {
		pids, err = own.Pidof(names...)
		if err != nil {
			return nil, err
		}

		if len(pids) >= len(names) {
			return pids, nil
		}

		<-time.After(1 * time.Second)
	}

	return pids, fmt.Errorf("[SSH] wait processes timeout %ds: %v", tiemout, pids)
}

type LsFile struct {
	Name   string `json:"name"`
	Size   int64  `json:"size"`
	Owner  string `json:"owner"`
	Group  string `json:"group"`
	IsFile bool   `json:"is_file"`
	Number int64  `json:"number"`
	Rights string `json:"rights"`
	Modify string `json:"modify"`
}

// SRN S-空格符；T-回车符；N-换行符
var filteSRTRegexp = regexp.MustCompile(`[^ \r\n]+`)

func (own *Client) Ls(filename string) ([]*LsFile, error) {
	cmd := "ls -la --color=never " + filename
	data, err := own.Execute(cmd)
	if err != nil {
		return nil, err
	}

	metas := strings.Split(data, "\n")
	files := make([]*LsFile, 0, len(metas))
	for _, meta := range metas {
		if len(meta) == 0 || strings.HasPrefix(meta, "total ") {
			continue
		}

		fields := strings.Split(strings.TrimRight(meta, "\r"), " ")
		if fields = Filter(fields, ""); len(fields) != 9 {
			continue
		}

		file := &LsFile{
			Name:   fields[8],
			Owner:  fields[2],
			Group:  fields[3],
			Rights: fields[0],
			IsFile: fields[0][0] == '-',
			Modify: joinLsDatetime(fields[5], fields[6], fields[7]),
		}

		file.Number, _ = strconv.ParseInt(fields[1], 10, 64)
		file.Size, _ = strconv.ParseInt(fields[4], 10, 64)
		files = append(files, file)
	}

	return files, nil
}

func joinLsDatetime(month, day, time string) string {
	switch month {
	case "Jan":
		month = "01"
	case "Feb":
		month = "02"
	case "Mar":
		month = "03"
	case "Apr":
		month = "04"
	case "May":
		month = "05"
	case "Jun":
		month = "06"
	case "Jul":
		month = "07"
	case "Aug":
		month = "08"
	case "Sep":
		month = "09"
	case "Oct":
		month = "10"
	case "Nov":
		month = "11"
	case "Dec":
		month = "12"
	}

	if len(time) == 4 {
		return fmt.Sprintf("%s-%s-%02s", time, month, day)
	}

	return fmt.Sprintf("%s-%02s %s", month, day, time)
}

type NetworkCard struct {
}

func (own *Client) Ifconfig() {}
