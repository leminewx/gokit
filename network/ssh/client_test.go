package ssh

import "testing"

const _TEST_DEVICE_IP = "172.22.67.199"

func TestClient_Execute(t *testing.T) {
	client := NewClient(_TEST_DEVICE_IP, "root", "Cictci@2022")
	if err := client.Connect(); err != nil {
		t.Fatal(err)
	}
	defer client.Close()

	if res, err := client.Execute("ls /system/bin"); err != nil {
		t.Fatal(err)
	} else {
		t.Log(res)
	}

	if res, err := client.Execute("ifconfig"); err != nil {
		t.Fatal(err)
	} else {
		t.Log(res)
	}

	if res, err := client.Execute("ps"); err != nil {
		t.Fatal(err)
	} else {
		t.Log(res)
	}
}

func TestClient_ExecuteWithTimeout(t *testing.T) {
	client := NewClient(_TEST_DEVICE_IP, "root", "Cictci@2022")
	if err := client.Connect(); err != nil {
		t.Fatal(err)
	}
	defer client.Close()

	res, err := client.ExecuteWithTimeout("ping 10.254.199.1", 7)
	if err != nil {
		t.Error(err)
	}

	t.Log(res)
}
