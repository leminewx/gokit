package ssh

import (
	"encoding/json"
	"fmt"
	"regexp"
	"testing"
)

func TestStart(t *testing.T) {
	client := NewClient(_TEST_DEVICE_IP, "root", "Cictci@2022")
	if err := client.Connect(); err != nil {
		t.Fatal(err)
	}
	defer client.Close()

	if _, err := client.Start(30, "omCore"); err != nil {
		t.Fatal(err)
	}

	if _, err := client.Start(30, "omCore"); err != nil {
		t.Fatal(err)
	}
}

func TestRestart(t *testing.T) {
	client := NewClient(_TEST_DEVICE_IP, "root", "Cictci@2022")
	if err := client.Connect(); err != nil {
		t.Fatal(err)
	}
	defer client.Close()

	pids, err := client.Restart(10, "omCore")
	if err != nil {
		t.Fatal(err)
	}
	t.Log(pids)
}

func TestLs(t *testing.T) {
	client := NewClient(_TEST_DEVICE_IP, "root", "Cictci@2022")
	if err := client.Connect(); err != nil {
		t.Fatal(err)
	}
	defer client.Close()

	files, err := client.Ls("/root")
	if err != nil {
		t.Fatal(err)
	}
	for _, file := range files {
		data, _ := json.Marshal(&file)
		fmt.Println(string(data))
	}

	files, err = client.Ls("123456")
	if err != nil {
		t.Fatal(err)
	}
	t.Log(files)
}

func TestRegexp(t *testing.T) {
	re := regexp.MustCompile(`\d+`)
	t.Log(re.FindAllString(" sdfsd sfsd d", -1))
	t.Log(re.FindAllString("123 12312, 123123 23,,23345345,", -1))
}
