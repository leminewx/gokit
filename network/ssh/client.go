package ssh

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"strings"
	"time"

	"golang.org/x/crypto/ssh"
)

const (
	DIAL_PORT    = 22
	DIAL_TIMEOUT = 3
)

type Client struct {
	port   int
	host   string
	addr   string
	print  bool
	config *ssh.ClientConfig
	client *ssh.Client
}

func NewClient(host, user, pwd string) *Client {
	return &Client{
		host:  host,
		port:  DIAL_PORT,
		addr:  fmt.Sprintf("%s:%d", host, DIAL_PORT),
		print: true,
		config: &ssh.ClientConfig{
			User:            user,
			Auth:            []ssh.AuthMethod{ssh.Password(pwd)},
			Timeout:         DIAL_TIMEOUT * time.Second,
			HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		},
	}
}

func (own *Client) WithPrint(print bool) *Client {
	own.print = print
	return own
}

func (own *Client) Connect(port ...int) (err error) {
	if len(port) > 0 {
		own.port = port[0]
		own.addr = fmt.Sprintf("%s:%d", own.host, own.port)
	}

	own.client, err = ssh.Dial("tcp", own.addr, own.config)
	return err
}

func (own *Client) Execute(cmd string) (string, error) {
	session, err := own.client.NewSession()
	if err != nil {
		return "", err
	}

	resp, err := session.CombinedOutput(cmd)
	return own.parseResponse(cmd, resp, err)
}

func (own *Client) ExecuteWithTimeout(cmd string, timeout int64) (string, error) {
	session, err := own.client.NewSession()
	if err != nil {
		return "", err
	}
	defer session.Close()

	var buf bytes.Buffer
	session.Stdout, session.Stderr = &buf, &buf
	if err = session.Start(cmd); err != nil {
		return "", err
	}

	go func() {
		<-time.After(time.Duration(timeout) * time.Second)
		session.Close()
	}()

	err = session.Wait()
	return own.parseResponse(cmd, buf.Bytes(), err)
}

func (own *Client) Close() error {
	return own.client.Close()
}

func (own *Client) parseResponse(cmd string, resp []byte, err error) (string, error) {
	data := string(bytes.TrimRight(bytes.TrimRight(bytes.TrimRight(resp, "\r\n"), "\n"), "\r"))
	if own.print {
		log.Printf("[SSH] [%s] %s\n%s", own.addr, cmd, data)
	}

	cmdName := strings.Split(cmd, " ")[0]
	if err == nil || err == io.EOF {
		return validateResponse(cmdName, data)
	}

	if errMsg := err.Error(); strings.HasSuffix(errMsg, "status 141") ||
		strings.HasSuffix(errMsg, "status 1") {
		return validateResponse(cmdName, data)
	}

	return data, fmt.Errorf("[SSH] [%s] error: %v", cmd, err)
}

const (
	ERR_KILLALL = "sh: you need to specify whom to kill"
)

func validateResponse(cmdName, resp string) (string, error) {
	switch cmdName {
	case "ls":
		if strings.HasPrefix(resp, "ls: ") {
			return "", fmt.Errorf(resp)
		}
	case "pidof":
		if strings.HasPrefix(resp, "pidof: ") {
			return "", fmt.Errorf(resp)
		}
	case "kill":
		if strings.HasPrefix(resp, "sh: can't kill pid") {
			return "", fmt.Errorf(resp)
		}
	case "killall":
		if strings.HasPrefix(resp, "sh: you need to specify") {
			return "", fmt.Errorf(resp)
		}
	case "find":
	default:
		if (strings.HasPrefix(resp, "sh: ") || strings.HasPrefix(resp, "bash: ")) && strings.HasSuffix(resp, "not found") {
			return "", fmt.Errorf(resp)
		}
	}

	return resp, nil
}
