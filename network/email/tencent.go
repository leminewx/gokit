package main

import (
	"crypto/tls"
	"net/smtp"
	"strings"
)

type Email struct {
	Host string   `json:"host"`
	Port int      `json:"port"`
	From string   `json:"from"`
	Pwd  string   `json:"pwd"`
	To   []string `json:"to"`
	Body []byte   `json:"message"`
}

func main() {
	panic(SendToMail("smtp.exmail.qq.com:465", "SnowLeopard@cictci.com", "Zxkzl2022", []string{"wenxian@cictci.com"}, "Test", "plain", []byte("hello tester\r\n")).Error())
}

func SendToMail(host, from, password string, to []string, subject, contentType string, body []byte) error {
	domain := strings.Split(host, ":")
	auth := smtp.PlainAuth("", from, password, domain[0])
	var content_type string
	if contentType == "html" {
		content_type = "Content-Type: text/html; charset=utf-8"
	} else {
		content_type = "Content-Type: text/plain; charset=utf-8"
	}

	msg := append([]byte("To: "+strings.Join(to, ";")+"\r\nFrom: "+from+"\r\nSubject: "+"\r\n"+content_type+"\r\n\r\n"), body...)
	// return smtp.SendMail(host, auth, from, to, msg)
	tlsConfig := &tls.Config{
		InsecureSkipVerify: true, // 不验证证书，生产环境不要这样做
	}

	// 发送邮件
	conn, err := tls.Dial("tcp", host, tlsConfig)
	if err != nil {
		return err
	}
	defer conn.Close()

	c, err := smtp.NewClient(conn, domain[0])
	if err != nil {
		return err
	}
	defer c.Close()

	if err := c.Auth(auth); err != nil {
		return err
	}

	if err := c.Mail(from); err != nil {
		return err
	}

	for _, addr := range to {
		if err := c.Rcpt(addr); err != nil {
			return err
		}
	}

	w, err := c.Data()
	if err != nil {
		return err
	}

	_, err = w.Write(msg)
	if err != nil {
		return err
	}

	err = w.Close()
	if err != nil {
		return err
	}

	err = c.Quit()
	if err != nil {
		return err
	}

	return nil
}
