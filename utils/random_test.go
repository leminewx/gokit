package utils

import "testing"

func BenchmarkGenerateRandomString(b *testing.B) {
	for i := 0; i < b.N; i++ {
		GenerateRandomString(20)
	}
}

func BenchmarkGenerateRandomNumericString(b *testing.B) {
	for i := 0; i < b.N; i++ {
		GenerateRandomNumericString(30)
	}
}
