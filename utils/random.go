package utils

import (
	"math/rand"
	"time"
)

var seed *rand.Rand

func init() {
	seed = rand.New(rand.NewSource(time.Now().UnixNano()))
}

var (
	_NUMBER_CHARS = []byte{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'}
	_LETTER_CHARS = []byte{
		'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
		'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
	}
	_MIX = []byte{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
		'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
		'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
	}
)

func GenerateRandomLetterString(size int) string {
	res := make([]byte, size)
	for i := range res {
		res[i] = _LETTER_CHARS[seed.Intn(52)]
	}

	return string(res)
}

func GenerateRandomNumericString(size int) string {
	res := make([]byte, size)
	for i := range res {
		res[i] = _NUMBER_CHARS[seed.Intn(10)]
	}

	return string(res)
}

func GenerateRandomString(size int) string {
	res := make([]byte, size)
	for i := range res {
		res[i] = _MIX[seed.Intn(62)]
	}

	return string(res)
}
