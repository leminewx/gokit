package main

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"testing"
)

func TestRequest(t *testing.T) {
	data, _ := json.Marshal(map[string]string{
		"username": "lemine@qq.com",
		"password": "123qwe!@#",
	})

	req, err := http.NewRequest(http.MethodPost, "http://10.254.249.225:9999/login", bytes.NewBuffer(data))
	if err != nil {
		panic(err)
	}

	// req.Header.Set("Content-Type", "application/json")
	// req.Header.Set("Content-Type", "multipart/form-data")
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	body, _ := io.ReadAll(resp.Body)
	t.Log(string(body))
}

func TestContentType(t *testing.T) {
	t.Log(http.DetectContentType([]byte("test.go")))
}
