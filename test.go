package main

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"gitee.com/leminewx/gokit/network/httpx"
)

func main() {
	http.HandleFunc("POST /login", func(resp http.ResponseWriter, req *http.Request) {
		ctx := httpx.NewContext(resp, req)
		ctx.ResponseJSON(http.StatusOK, map[string]any{
			"network":  ctx.GetFormValue("netcode"),
			"username": ctx.GetPostFormValue("username"),
			"password": ctx.GetMultipartFormValue("password"),
		})
	})

	http.HandleFunc("GET /file", func(resp http.ResponseWriter, req *http.Request) {
		ctx := httpx.NewContext(resp, req)
		ctx.ResponseFile(ctx.GetQueryValue("filename"))
	})

	http.HandleFunc("GET /communication", func(resp http.ResponseWriter, req *http.Request) {
		ctx := httpx.NewContext(resp, req)
		fmt.Println("start sse...")
		ctx.ResponseSSE(func() <-chan []byte {
			data := make(chan []byte, 1)
			go func() {
				for i := 0; i < 10; i++ {
					data <- []byte("hello")
					time.Sleep(3 * time.Second)
				}
				close(data)
			}()

			return data
		})
		fmt.Println("close sse")
	})

	log.Fatalln(http.ListenAndServe(":9999", nil))
}
