package jwt

type Algorithm string

const (
	ALG_HS256 Algorithm = "HS256"
)

type Type string

const (
	TYPE_JWT Type = "JWT"
)
