package jwt

import (
	"testing"
	"time"
)

func TestJwt(t *testing.T) {
	jwt := NewGenerator([]byte("123qwe!@#")).WithIssuer("gokit").WithExpirationTime(100).WithNotBefore(10)
	token, err := jwt.Encode(map[string]any{"uid": "lemine", "gid": 1})
	if err != nil {
		t.Fatal(err)
	}
	t.Log(token)

	time.Sleep(9 * time.Second)
	t.Log(jwt.Decode(token))

	time.Sleep(1 * time.Second)
	t.Log(jwt.Decode(token))

	time.Sleep(60 * time.Second)
	t.Log(jwt.Decode(token))
}

// BenchmarkJwt_Encode-80    	  110474	     22224 ns/op	    2754 B/op	      38 allocs/op
func BenchmarkJwt_Encode(b *testing.B) {
	jwt := NewGenerator([]byte("123qwe!@#")).WithIssuer("gokit").WithExpirationTime(60).WithNotBefore(10)
	for i := 0; i < b.N; i++ {
		jwt.Encode(map[string]any{"uid": "lemine", "gid": 1})
	}
}

// BenchmarkJwt_Decode-80    	  143472	      8297 ns/op	    1944 B/op	      41 allocs/op
func BenchmarkJwt_Decode(b *testing.B) {
	jwt := NewGenerator([]byte("123qwe!@#")).WithIssuer("gokit").WithExpirationTime(100)
	token, err := jwt.Encode(map[string]any{"uid": "lemine", "gid": 1})
	if err != nil {
		b.Fatal(err)
	}
	for i := 0; i < b.N; i++ {
		if _, err := jwt.Decode(token); err != nil {
			b.Fatal(err)
		}
	}
}
