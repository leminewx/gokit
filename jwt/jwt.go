package jwt

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"strings"
	"sync"
	"time"

	"gitee.com/leminewx/gokit/utils"
)

const (
	DEFAULT_EXPIRED_DURATION = 60 * 60 * 24 * 7 // a week
)

type JWTGenerator struct {
	// header claims
	alg Algorithm
	typ Type

	// a part of standard claims
	iss string // issuer
	sub string // subject
	exp int64  // expiration time
	nbf int64  // not before

	// salt
	key []byte

	header []byte
	jtis   map[any]struct{}
	lock   sync.RWMutex
}

func NewGenerator(key []byte) *JWTGenerator {
	header, _ := json.Marshal(map[string]any{"alg": ALG_HS256, "typ": TYPE_JWT})
	return &JWTGenerator{
		alg:    ALG_HS256,
		typ:    TYPE_JWT,
		key:    key,
		exp:    DEFAULT_EXPIRED_DURATION,
		jtis:   make(map[any]struct{}),
		header: header,
	}
}

func (own *JWTGenerator) WithAlgorithm(algorithm Algorithm) *JWTGenerator {
	own.lock.Lock()
	own.alg = algorithm
	own.header, _ = json.Marshal(map[string]any{"alg": algorithm, "typ": own.typ})
	own.lock.Unlock()
	return own
}

func (own *JWTGenerator) WithIssuer(issuer string) *JWTGenerator {
	own.lock.Lock()
	own.iss = issuer
	own.lock.Unlock()
	return own
}

func (own *JWTGenerator) WithSubject(subject string) *JWTGenerator {
	own.lock.Lock()
	own.sub = subject
	own.lock.Unlock()
	return own
}

func (own *JWTGenerator) WithNotBefore(seconds int64) *JWTGenerator {
	own.lock.Lock()
	own.nbf = seconds
	own.lock.Unlock()
	return own
}

func (own *JWTGenerator) WithExpirationTime(seconds int64) *JWTGenerator {
	if seconds >= 60 {
		own.lock.Lock()
		own.exp = seconds
		own.lock.Unlock()
	}

	return own
}

func (own *JWTGenerator) Encode(playload map[string]any) (signature string, err error) {
	var dataBytes []byte
	own.initPlayload(playload)
	if dataBytes, err = json.Marshal(&playload); err != nil {
		return "", fmt.Errorf("marshal playload error: %v", err)
	}

	encodedHeader := base64.RawURLEncoding.EncodeToString(own.header)
	encodedPayload := base64.RawURLEncoding.EncodeToString(dataBytes)
	HeaderAndPayload := encodedHeader + "." + encodedPayload
	switch own.alg {
	case ALG_HS256:
		if signature, err = generateSHA256Signature(own.key, []byte(HeaderAndPayload)); err != nil {
			own.lock.Unlock()
			return "", fmt.Errorf("generate sha256 signature error: %v", err)
		}
	}

	return HeaderAndPayload + "." + signature, nil
}

func (own *JWTGenerator) initPlayload(playload map[string]any) error {
	if own.iss != "" {
		playload["iss"] = own.iss
	}
	if own.sub != "" {
		playload["sub"] = own.sub
	}

	now := time.Now().Unix()
	playload["exp"] = now + own.exp
	if own.nbf != 0 {
		playload["nbf"] = now + own.nbf
	}

	jti := utils.GenerateRandomString(20)
	playload["jti"] = jti
	own.lock.Lock()
	own.jtis[playload["jti"]] = struct{}{}
	own.lock.Unlock()
	return nil
}

func (own *JWTGenerator) Decode(token string) (map[string]any, error) {
	jwtParts := strings.Split(token, ".")
	if len(jwtParts) != 3 {
		return nil, fmt.Errorf("jwt is illegal")
	}

	encodedPayload := jwtParts[1]
	playloadBytes, err := base64.RawURLEncoding.DecodeString(encodedPayload)
	if err != nil {
		return nil, fmt.Errorf("jwt[playload] is illegal")
	}

	var playload map[string]any
	if err := json.Unmarshal(playloadBytes, &playload); err != nil {
		return nil, fmt.Errorf("jwt[playload] is invalid")
	} else if err := own.validatePlayload(playload); err != nil {
		return nil, err
	}

	signature := jwtParts[2]
	encodedHeader := jwtParts[0]
	if confirmSignature, _ := generateSHA256Signature(own.key, []byte(encodedHeader+"."+encodedPayload)); signature != confirmSignature {
		return nil, fmt.Errorf("jwt is invalid")
	}

	return playload, nil
}

func (own *JWTGenerator) validatePlayload(playload map[string]any) error {
	now := time.Now().Unix()
	if exp, ok := playload["exp"].(float64); !ok {
		return fmt.Errorf("jwt[exp] is empty")
	} else if now > int64(exp) {
		own.lock.Lock()
		delete(own.jtis, playload["jti"])
		own.lock.Unlock()
		return fmt.Errorf("jwt[exp] is expired")
	}

	if jti, ok := playload["jti"]; !ok {
		return fmt.Errorf("jwt[jti] is empty")
	} else {
		own.lock.RLock()
		_, ok = own.jtis[jti]
		if own.lock.RUnlock(); !ok {
			return fmt.Errorf("jwt[jti] is invalid")
		}
	}

	if own.iss != "" {
		if iss, ok := playload["iss"]; !ok {
			return fmt.Errorf("jwt[iss] is empty")
		} else if iss != own.iss {
			return fmt.Errorf("jwt[iss] is invalid")
		}
	}

	if own.nbf > 0 {
		if nbf, ok := playload["nbf"].(float64); !ok {
			return fmt.Errorf("jwt[nbf] is illegal")
		} else if now < int64(nbf) {
			return fmt.Errorf("jwt[nbf] cannot be used yet")
		}
	}

	return nil
}

func generateSHA256Signature(key []byte, data []byte) (string, error) {
	hash := hmac.New(sha256.New, key)
	if _, err := hash.Write(data); err != nil {
		return "", err
	}

	return base64.RawURLEncoding.EncodeToString(hash.Sum(nil)), nil
}
