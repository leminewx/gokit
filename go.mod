module gitee.com/leminewx/gokit

go 1.22.5

require (
	github.com/go-sql-driver/mysql v1.8.1
	golang.org/x/crypto v0.27.0
)

require (
	filippo.io/edwards25519 v1.1.0 // indirect
	golang.org/x/sys v0.25.0 // indirect
)
