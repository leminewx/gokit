package pool

import (
	"bytes"
	"sync"
)

type ByteBufferPool sync.Pool

func NewByteBufferPool() *ByteBufferPool {
	return &ByteBufferPool{
		New: func() any { return new(bytes.Buffer) },
	}
}

func (own *ByteBufferPool) Alloc() *bytes.Buffer {
	return (*sync.Pool)(own).Get().(*bytes.Buffer)
}

func (own *ByteBufferPool) Free(buf *bytes.Buffer) {
	buf.Reset()
	(*sync.Pool)(own).Put(buf)
}
