package pool

import (
	"strings"
	"testing"
)

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/gokit/pool
// cpu: QEMU Virtual CPU version 2.5+
// BenchmarkStringBuilderPool-80    	18507374	        59.92 ns/op	      24 B/op	       2 allocs/op
func BenchmarkStringBuilder(b *testing.B) {
	for i := 0; i < b.N; i++ {
		buf := new(strings.Builder)
		buf.WriteString("hello ")
		buf.WriteString("world ")
		buf.WriteByte('\n')
	}
}
