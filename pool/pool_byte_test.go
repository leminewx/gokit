package pool

import (
	"bytes"
	"testing"
)

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/gokit/pool
// cpu: QEMU Virtual CPU version 2.5+
// BenchmarkByteBuffer-80    	20061523	        59.72 ns/op	      64 B/op	       1 allocs/op
func BenchmarkByteBuffer(b *testing.B) {
	for i := 0; i < b.N; i++ {
		buf := new(bytes.Buffer)
		buf.WriteString("hello ")
		buf.WriteString("world ")
		buf.WriteByte('\n')
	}
}
