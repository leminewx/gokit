package pool

import "sync"

type SlicePointerPool sync.Pool

func NewSlicePointerPool(size int) *SlicePointerPool {
	return &SlicePointerPool{
		New: func() any {
			buf := make([]byte, 0, size)
			return &buf
		},
	}
}

func (own *SlicePointerPool) Alloc() *[]byte {
	return (*sync.Pool)(own).Get().(*[]byte)
}

const maxSlicePointerBufferSize = 4 << 10

func (own *SlicePointerPool) Free(buf *[]byte) {
	// to reduce peak allocation, return only smaller buffers to the pool.
	if cap(*buf) <= maxSlicePointerBufferSize {
		*buf = (*buf)[:0]
		(*sync.Pool)(own).Put(buf)
	}
}
