package pool

type Type int8

const (
	TYPE_BYTE_BUFFER Type = iota
	TYPE_SLICE_POINTER
	TYPE_STRING_BUILDER
)
