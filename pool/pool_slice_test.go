package pool

import (
	"testing"
)

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/gokit/pool
// cpu: QEMU Virtual CPU version 2.5+
// BenchmarkSliceAppend-80    	88404777	        13.44 ns/op	       0 B/op	       0 allocs/op
func BenchmarkSliceAppend(b *testing.B) {
	for i := 0; i < b.N; i++ {
		buf := make([]byte, 0, 1024)
		buf = append(buf, "hello "...)
		buf = append(buf, "world "...)
		_ = append(buf, '\n')
	}
}
