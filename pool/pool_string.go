package pool

import (
	"strings"
	"sync"
)

type StringBuilderPool sync.Pool

func NewStringBuilderPool() *StringBuilderPool {
	return &StringBuilderPool{
		New: func() any { return new(strings.Builder) },
	}
}

func (own *StringBuilderPool) Alloc() *strings.Builder {
	return (*sync.Pool)(own).Get().(*strings.Builder)
}

func (own *StringBuilderPool) Free(buf *strings.Builder) {
	buf.Reset()
	(*sync.Pool)(own).Put(buf)
}
