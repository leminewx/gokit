package pool

type Pool[T any] interface {
	Alloc() T
	Free(T)
}

var (
	DefualtByteBufferPool    = NewByteBufferPool()
	DefaultStringBuilderPool = NewStringBuilderPool()
)
