package validator

import "regexp"

var (
	emailReg       = regexp.MustCompile(`^([\w\.\_]{2,10})@(\w{1,}).([a-z]{2,4})$`)
	englishReg     = regexp.MustCompile(`^[a-zA-Z]+$`)
	chineseReg     = regexp.MustCompile(`^[\\x{4e00}-\\x{9fa5}]+$`)
	phoneNumberReg = regexp.MustCompile(`^(1[3|4|5|8][0-9]\d{4,8})$`)
)

func IsEmail(val string) bool {
	return emailReg.MatchString(val)
}

func IsEnglish(val string) bool {
	return englishReg.MatchString(val)
}

func IsChinese(val string) bool {
	return chineseReg.MatchString(val)
}

func IsPhoneNumber(val string) bool {
	return phoneNumberReg.MatchString(val)
}
